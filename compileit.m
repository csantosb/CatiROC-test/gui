addpath(fullfile (getcurrentdir, 'ipeak7'))

if strcmp('win64',computer('arch'))

    mcc -m catirocgui.m -a ipeak.m -a save_data.m -a RunIpeak.m -a libftdi_x86_4_0_prototype.m -a libftdi_x86_4_0.dll -a libftdi_thunk_pcwin64.dll
    mcc -m displayfig
    mcc -m decodedata

elseif strcmp('glnxa64',computer('arch'))

    mcc -m catirocgui.m -a ipeak.m -a save_data.m -a RunIpeak.m -a libftdi_linux_prototype.m -a libftdi_linux.so -a libftdi_thunk_glnxa64.so
    mcc -m displayfig
    mcc -m decodedata

end