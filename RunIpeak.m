function RunIpeak()

    xx = get(gco,'XData');
    yy = get(gco,'YData');
%     xx = xx(find(yy~=0,1,'first'):find(yy~=0,1,'last'));
%     yy = yy(find(yy~=0,1,'first'):find(yy~=0,1,'last'));
    % whos xx yy

    ipeak(double(xx), double(yy));
    % set(gcf, 'CloseRequestFcn', 'closereq')

    fprintf('\n\n\t IPeak\n ')
    fprintf('\n\tCredits to: Professor Tom O''Haver\n')
    fprintf('\tDepartment of Chemistry and Biochemistry\n')
    fprintf('\tUniversity of Maryland at College Park\n\n')

    fprintf('\thttps://www.mathworks.com/matlabcentral/fileexchange/11755-peak-finding-and-measurement\n')
    fprintf('\thttps://terpconnect.umd.edu/~toh/spectrum/\n')
    fprintf('\thttps://terpconnect.umd.edu/~toh/spectrum/PeakFindingandMeasurement.htm\n')
    fprintf('\thttps://terpconnect.umd.edu/~toh/spectrum/ifpinstructions.html\n\n')