function decodedata()

[filename, pathname, filterindex] = ...
    uigetfile( {'*.bin','Data (*.bin)'}, 'Pick a file');

fid = fopen(filename, 'r');
data = fread(fid, inf, 'uint8');
data_ = reshape(data, 8, [])';
datadecoded = data_(1:200, :)
fclose(fid);
fprintf('\nData format is one 64 bits (8 bytes) event by line\n')