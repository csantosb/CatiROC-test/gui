#+TITLE: CatiROC test gui

This project is part of the =CatiROC-test= [[https://gitlab.in2p3.fr/groups/CatiROC-test][group]] of projects. They all provide the
code necessary for testing the CatiROC ASIC from [[http://omega.in2p3.fr/][Omega]] in two flavours

 - Omega test board
 - Juno Mezzanine board

[[https://gitlab.in2p3.fr/CatiROC-test/gui][This]] project comprises all the high level MATLAB source code to be used with
these two boards, with two backups [[https://gitlab.com/CatiROC-test/gui][here]] and [[https://gitlab.cern.ch/CatiROC-test/gui][here]].

More information may be found at the [[https://catiroc-test.pages.in2p3.fr/gui/][page]] of the project.

* License

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the [[http://www.gnu.org/licenses/gpl.txt][GNU General Public License]] along with
this program. If not, see [[http://www.gnu.org/licenses/gpl.html][http://www.gnu.org/licenses/gpl.html]].
