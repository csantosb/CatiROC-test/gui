function save_data()

    data = [get(gco,'XData')', get(gco,'YData')'];
    [filename, pathname] = uiputfile('data.txt', 'Save Data as ... ');
    if isempty(filename)
        return
    end
    fid = fopen([pathname, filename], 'w');
    fprintf(fid, '%i  %i\n', data');
    fclose(fid);