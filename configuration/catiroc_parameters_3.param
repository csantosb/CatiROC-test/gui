#
# CatiROC Configuration file
#
# File format
#
#     - VARIABLE = VALUE; # COMMENT
#     - This line is a comment: all text after # is considered as a comment
#     - Empty lines are ignored

# The "_format" suffix in "VARIABLE_format" stands for the format representation of VARIABLE
# Valid options are 'bin', 'hex', dec
# Example:
#           Variable = 10 (dec)
#           Variable = 1010 (bin, most significant bit to the left)
#           Variable = A (hex, most significant bit to the left)

# WARNING: in binary fields, most significant bit goes to the left so that
#          bin"1000"/hex"8" (and not bin"0001"/hex"1" !!) is equivalent to
#          decimal 8
#
# For Omega, a 16 bits register A from bit 0 to bit 15 is assigned as follows:
#
#   Bit 0  contient le bit de poids fort
#   Bit 15 contient le bit de poids faible
#
# When decoding this file, this fact is deal with automatically.


## ################### SLOW CONTROL REGISTER #######################################

## [#0] - Enable input test, 16 bits
# 0 = disable, 1= enable
# channel 15 to 0 <-> bits 15 to 0
EnableInputTestChannel.format = hex;
EnableInputTestChannel.value = 0000;

## [#16] - Gain Preamp (1pF;0.5pF, 0.25pF, ….8pF), 10 bits
## switch off PA HG
## switch off PA LG

# [#16] - CH0 - sw_cf_ch0_<7:0>
sw_cf(1).format = bin;
sw_cf(1).value = 00100000;
sw_cf(1).off_PA_HG = 1;
sw_cf(1).off_PA_LG = 1;

# [#26] - CH1 - sw_cf_ch1_<7:0>
sw_cf(2).format = bin;
sw_cf(2).value = 00100000;
sw_cf(2).off_PA_HG = 1;
sw_cf(2).off_PA_LG = 1;

# [#36] - CH2 - sw_cf_ch2_<7:0>
sw_cf(3).format = bin;
sw_cf(3).value = 00100000;
sw_cf(3).off_PA_HG = 1;
sw_cf(3).off_PA_LG = 1;

# [#46] - CH3 - sw_cf_ch3_<7:0>
sw_cf(4).format = bin;
sw_cf(4).value = 00100000;
sw_cf(4).off_PA_HG = 1;
sw_cf(4).off_PA_LG = 1;

# [#56] - CH4 - sw_cf_ch4_<7:0>
sw_cf(5).format = bin;
sw_cf(5).value = 00100000;
sw_cf(5).off_PA_HG = 1;
sw_cf(5).off_PA_LG = 1;

# [#66] - CH5 - sw_cf_ch5_<7:0>
sw_cf(6).format = bin;
sw_cf(6).value = 00100000;
sw_cf(6).off_PA_HG = 1;
sw_cf(6).off_PA_LG = 1;

# [#76] - CH6 - sw_cf_ch6_<7:0>
sw_cf(7).format = bin;
sw_cf(7).value = 00100000;
sw_cf(7).off_PA_HG = 1;
sw_cf(7).off_PA_LG = 1;

# [#86] - CH7 - sw_cf_ch7_<7:0>
sw_cf(8).format = bin;
sw_cf(8).value = 00100000;
sw_cf(8).off_PA_HG = 1;
sw_cf(8).off_PA_LG = 1;

# [#96] - CH8 - sw_cf_ch8_<7:0>
sw_cf(9).format = bin;
sw_cf(9).value = 00100000;
sw_cf(9).off_PA_HG = 1;
sw_cf(9).off_PA_LG = 1;

# [#106] - CH9 - sw_cf_ch9_<7:0>
sw_cf(10).format = bin;
sw_cf(10).value = 00100000;
sw_cf(10).off_PA_HG = 1;
sw_cf(10).off_PA_LG = 1;

# [#116] - CH10 - sw_cf_ch10_<7:0>
sw_cf(11).format = bin;
sw_cf(11).value = 00100000;
sw_cf(11).off_PA_HG = 1;
sw_cf(11).off_PA_LG = 1;

# [#126] - CH11 - sw_cf_ch11_<7:0>
sw_cf(12).format = bin;
sw_cf(12).value = 00100000;
sw_cf(12).off_PA_HG = 1;
sw_cf(12).off_PA_LG = 1;

# [#136] - CH12 - sw_cf_ch12_<7:0>
sw_cf(13).format = bin;
sw_cf(13).value = 00100000;
sw_cf(13).off_PA_HG = 1;
sw_cf(13).off_PA_LG = 1;

# [#146] - CH13 - sw_cf_ch13_<7:0>
sw_cf(14).format = bin;
sw_cf(14).value = 00100000;
sw_cf(14).off_PA_HG = 1;
sw_cf(14).off_PA_LG = 1;

# [#156] - CH14 - sw_cf_ch14_<7:0>
sw_cf(15).format = bin;
sw_cf(15).value = 00100000;
sw_cf(15).off_PA_HG = 1;
sw_cf(15).off_PA_LG = 1;

# [#166] - CH15 - sw_cf_ch15_<7:0>
sw_cf(16).format = bin;
sw_cf(16).value = 00100000;
sw_cf(16).off_PA_HG = 1;
sw_cf(16).off_PA_LG = 1;


## [#176] - Power on preamp HG and LG for all channels, 2 bits
PowerOn.Preamp.HG.en = 1;
PowerOn.Preamp.HG.pp = 0;


## [#178] - Power on preamp HG and LG for all channels, 2 bits
PowerOn.Preamp.LG.en = 1;
PowerOn.Preamp.LG.pp = 0;


## [#180] - Power on slow shaper high gain for all channels, 2 bits
SlowShapper.PowerOn.HG.en = 1;   # 10bin à SSH HG on
SlowShapper.PowerOn.HG.pp = 0;   # 10bin à SSH HG on


## [#182] - Gain of slow shaper HG, 2 bits
# cmd_g_hg_<1:0>
SlowShapper.Gain.HG.format = dec;
SlowShapper.Gain.HG.value = 2;       # 00 bin à Rf1 = 64K W


## [#184] - Time constant of 2nd stage slow shaper high gain, 3 bits
# cmd_rc_hg_<2:0>
SlowShapper.TimeConstant.HG.format = dec;
SlowShapper.TimeConstant.HG.value = 2;       # 010bin à RC= 50ns


## [#187] - Power on slow shaper low gain for all channels, 2 bits
SlowShapper.PowerOn.LG.en = 1;  # 10bin à SSH LG on
SlowShapper.PowerOn.LG.pp = 0;  # 10bin à SSH LG on


## [#189] - Gain of slow shaper LG, 2 bits
# cmd_g_lg_<1:0>
SlowShapper.Gain.LG.format = dec;
SlowShapper.Gain.LG.value = 2;       # 00bin à Rf1 = 64K W


## [#191] - Time constant of 2nd stage slow shaper low gain, 3 bits
# cmd_rc_lg_<2:0>
SlowShapper.TimeConstant.LG.format = dec;
SlowShapper.TimeConstant.LG.value = 2;       # 010bin à RC= 50ns


## [#194] - Feedback capa of 1st stage slow shaper high gain, 3 bits
# cmd_rcg_hg_<2:0>
SlowShapper.Feedback.HG.format = dec;
SlowShapper.Feedback.HG.value = 1;       # 001bin à Cf1= 1.5pF


## [#197] - Feedback capa of 1st stage slow shaper low gain, 3 bits
# cmd_rcg_hg_<2:0>
SlowShapper.Feedback.LG.format = dec;
SlowShapper.Feedback.LG.value = 1;       # 001bin à Cf1= 1.5pF


## [#200] - power on SCA for all channels, 2 bits
SlowShapper.PowerOnSCA.en = 1;  # 10 à SCA on
SlowShapper.PowerOnSCA.pp = 0;  # 10 à SCA on


## [#202] - gain mode selection
# (forced =0, auto=1), forced gain chosen by ch
ModeSelection.Gain = 0; # 1 à mode auto gain


## [#203] - power on gain_discri for all channels, 2 bits
PowerOn.GainDiscri.en = 1;  # 10 à gain_discri on
PowerOn.GainDiscri.pp = 0;  # 10 à gain_discri on


## [#205] - Power on fast shaper for all channels, 2 bits
FastShaper.PowerOn.en = 1;  # 10 à fast shaper on
FastShaper.PowerOn.pp = 0;  # 10 à fast shaper on


## [#207] - HOLDb mode selection, for all channels, 1 bit
# (internal =0, external=1)
ModeSelection.Hold = 0; # 0 à mode int. HOLDb


## [#208] - Power on time_discri for all channels, 2 bits
PowerOn.TimeDiscri.en = 1;  # 10 à time_discri on
PowerOn.TimeDiscri.pp = 0;  # 10 à time_discri on


## [#210] - Power on TDC for all channels, 2 bits
PowerOn.TDC.en = 1;  # 10 à TDC on
PowerOn.TDC.pp = 0;  # 10 à TDC on


## [#212] - Power on charge_ADC for all channels, 2 bits
PowerOn.ChargeADC.en = 1;  # 10 à Charge_ADC on
PowerOn.ChargeADC.pp = 0;  # 10 à Charge_ADC on


## [#214] - Power on time_ADC for all channels, 2 bits
PowerOn.TimeADC.en = 1;  # 10 à Time_ADC on
PowerOn.TimeADC.pp = 0;  # 10 à Time_ADC on


## [#216] - Multiplexed data at both ADC inputs, ADCs calibration with external input, 2 bits
# 00 : Q and T
# 10 : T and Q
# 01 : HG and LG charge
# 11 : ADCs calibration
# Sel_data_<1:0>
SelData.format = dec;
SelData.value = 0;           # 0 for both à Q on charge, ADC and T on time ADC


## [#218] - Power on DAC for Delay for all channels, 2 bits
PowerOn.DACDelay.en = 1;     # 10 à DAC_delay on
PowerOn.DACDelay.pp = 0; # 10 à DAC_delay on


## [#220] - Delay value (from MSB to LSB), for all channels, 8 bits
# Delay_<7:0>
Delay.format = bin;
Delay.value = 00011110;     # delay<4> + delay<1>


## [#228] - Mask of the time discri, 16 bits
# from ch15 to ch0, 0 = no mask, 1 = masked
# channel 15 to 0 <-> bits 15 to 0
# Mask_<15:0>
Mask.format = hex;
Mask.value = 0000;     # no ch.  masked


## [#244] - Choice slow shaper sent to pad and gain value in case of forced gain for ch 0 to 15, 32 bits
# HG (0) or LG (1) sent to pad and LG (0) or HG (1) used as forced gain for each channel
# 2 bits per channel 1 to 16

# CH1
SlowShapper.Sent(1).format = dec;
SlowShapper.Sent(1).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH2
SlowShapper.Sent(2).format = dec;
SlowShapper.Sent(2).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH3
SlowShapper.Sent(3).format = dec;
SlowShapper.Sent(3).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH4
SlowShapper.Sent(4).format = dec;
SlowShapper.Sent(4).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH5
SlowShapper.Sent(5).format = dec;
SlowShapper.Sent(5).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH6
SlowShapper.Sent(6).format = dec;
SlowShapper.Sent(6).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH7
SlowShapper.Sent(7).format = dec;
SlowShapper.Sent(7).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH8
SlowShapper.Sent(8).format = dec;
SlowShapper.Sent(8).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH9
SlowShapper.Sent(9).format = dec;
SlowShapper.Sent(9).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH10
SlowShapper.Sent(10).format = dec;
SlowShapper.Sent(10).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH11
SlowShapper.Sent(11).format = dec;
SlowShapper.Sent(11).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH12
SlowShapper.Sent(12).format = dec;
SlowShapper.Sent(12).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH13
SlowShapper.Sent(13).format = dec;
SlowShapper.Sent(13).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH14
SlowShapper.Sent(14).format = dec;
SlowShapper.Sent(14).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH15
SlowShapper.Sent(15).format = dec;
SlowShapper.Sent(15).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

# CH16
SlowShapper.Sent(16).format = dec;
SlowShapper.Sent(16).value = 0;  # 16 x 00 à HG to pad and LG as forced gain

## [#276] - Power on DACs  for all channels, 2 bits
PowerOn.DAC.en = 1;  # 10bin à both DAC on
PowerOn.DAC.pp = 0;  # 10bin à both DAC on


## [#278] - 10-bit DAC (MSB-LSB) for discri_time, 10 bits, csb
# discri_trigger_<9:0>
Discri.TimeDAC.format = dec;
Discri.TimeDAC.value = 900;

## [#288] - 10-bit DAC (MSB-LSB) for discri_charge, 10 bits
# discri_gain_<9:0>
Discri.ChargeDAC.format = dec;
Discri.ChargeDAC.value = 750;

## [#298] - Power on ramp for charge ADC, 2 bits
PowerOn.RampChargeADC.en = 1;  # 10bin à ramp on
PowerOn.RampChargeADC.pp = 0;  # 10bin à ramp on


## [#300] - Add compensated switches on charge ADC ramp, 1 bit
# (useb = 0 with switch, 1 no switch)
AddCompensatedSwitches.ChargeADCRamp = 1; # 1 without switch


## [#301] - Choice charge ADC ramp slope for 80MHZ (1) or 160MHz(0)ADC, 1 bit
ChargeADCRampSlope = 0; # 0 ramp for 160MHz ADC


## [#302] - Power on ramp  for time ADC, 2 bits
PowerOn.RampTimeADC.en = 1;  # 10bin à ramp on
PowerOn.RampTimeADC.pp = 0;  # 10bin à ramp on


## [#304] - Add compensated switches on time ADC ramp, 1 bit
# (useb = 0 with switch, 1 no switch)
AddCompensatedSwitches.TimeADCRamp = 1; # 1 without switch


## [#305] - Choice time ADC ramp slope for 80MHZ (1) or 160MHz(0) ADC, 1 bit,
TimeADCRampSlope = 0; # 0 ramp for 160MHz ADC


## [#306] - Power on OTAs for charge outputs, 2 bits
PowerOn.OTA.en = 1;  # 10bin à OTAs on
PowerOn.OTA.pp = 0;  # 10bin à OTAs on


## [#308] - Switch off OTA for probe output
# (0 = OFF, 1=ON)
SwitchOff.OTA = 1;  # 1 à OTA on


## [#309] - Power on slow_lvds_receiver, 2 bits
PowerOn.SlowLVDSReceiver.en = 1;  # 10bin à slow lvds rec on
PowerOn.SlowLVDSReceiver.pp = 0;  # 10bin à slow lvds rec on


## [#311] - Switch off 40 MHZ lvds_receiver, 1 bit
# (0 = OFF, 1=ON)
SwitchOff.LVDSReceiver40MHz = 1;  # 1 à 40 MHZ receiver on


## [#312] - Switch off 160MHZ lvds_receiver, 1 bit
# (0 = OFF, 1=ON)
SwitchOff.LVDSReceiver160MHz = 1;   # 1 à 160 MHZ receiver on csb


## [#313] - Sel external (0) or internal (1) 40MHz, 1 bit
# int : 160MHz/4, ext : LVDS RX
SelClkDiv4 = 0;  # 1 à 160MHz / 4


## [#314] - Sel RO_clk (0= input clk, 1 = input clk/2), 1 bit
# RO_clk at 80MHZ
Sel80M = 1; # 1 à 160MHz / 2


## [#315] - Disable buffer for Ovf of TS counter : 0 = en, 1 = dis, 1 bit
DisableOvfCpt = 0;  # 1 à buffer disable


## [#316] - Sel ext Raz channel, 1 bit
# 0= internal Raz, 1= external Raz
SelExtRazChannel = 0;  # 0 à internal Raz


## [#317] - Not Used


## [#318] - Sel ext Read
# 0= internal Read, 1= external Read, 1 bit
SelExtRead = 0;  # 0 à internal Read


## [#319] - Enable readout of Tac data, 1 bit
# 0 = suppress Data, 1 = data readout
EnableTacReadout = 1;  # 0 à no data


## [#320] - Enable output buffer for NOR16, 1 bit
# 0= suppress Data, 1= data readout
EnableNOR16 = 1; # 1 à enable readout


## [#321] - Enable output buffers for transmit on, 1 bit
# 0= suppress Data, 1= data readout
EnableTransmit = 1; # 1 à enable readout


## [#322] - Enable output buffers for data, 1 bit
# 0 = suppress Data, 1 = data readout
EnableData_oc = 1; # 1 à enable readout


## [#323] - Disable buffers for triggers, 1 bit
# 0 = enable, 1 =  disable
DisableTriggerBuffer = 0; # 0 à enable triggers


## [#324] - Power on lvds transceiver, 2 bits
PowerOn.LVDSTransceiver.en = 1;  # 10bin lvds transceiver on
PowerOn.LVDSTransceiver.pp = 0;  # 10bin lvds transceiver on


## [#326] - Switch 1mA or 2 mA in TX bias (on_1mA + on_2mA), 2 bits
# 0 = OFF, 1 = ON
SwitchTxBias.format = dec;  # 11 à +1mA+2mA in bias
SwitchTxBias.value = 3;     # 11 à +1mA+2mA in bias

## ################### PROBE REGISTER #######################################

## [#0] - probe data, ADC input signals, (data_Q and data_T) for ch 0 to ch 15, 32 bits
probe.data.format = hex;
probe.data.value = 00000000;


## [#32] - probe dig 2, ReadSCA0 and RazChnb for ch0 to ch15, 32 bits
probe.dig2.format = dec;
probe.dig2.value = 0;


## [#64] - fast shaper output for ch 0 to ch 15, 16 bits
probe.fsh.format = dec;
probe.fsh.value = 0;


## [#80] - probe dig 1, THb_SCA0_Q, THb_SCA1_Q, ValGain for ch0 to ch15, 48 bits
probe.dig1.format = dec;
probe.dig1.value = 0;


## [#128] - Slow shaper HG + slow shaper LG outputs for ch 0 to ch 15, 32 bits
probe.ssh.format = dec;
probe.ssh.value = 0;


## [#160] - preamp HG + preamp LG outputs for ch 0 to ch 15, 32 bits
probe.pa.format = hex;
probe.pa.value = 00000000;


## [#192] - LoadValGain, 1 bit
probe.dig0 = 0;


## [#193] - StartAdcRampb, 1 bit
probe.dig4 = 0;
