* Table of Contents                                 :TOC:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#10][1.0]]
  - [[#folder-names-are-not-random][Folder names are not random]]
  - [[#update-doc][Update doc]]
  - [[#avoid-visualization-when-saving-data][Avoid visualization when saving data]]

* 1.0

** DONE Folder names are not random
CLOSED: [2017-07-18 mar. 14:07]

- CLOSING NOTE

  Fixed in this [[orgit-rev:~/Projects/apc/juno/CatiROC-test-gui/::b3a8787][commit]].

  ----------------------------------------------------------------

When saving data on disk, the random folder name is not that random in
stand alone mode.

** TODO Update doc

** TODO Avoid visualization when saving data

In order to optimize cpu time, and to avoid any possible data loose,
saving data must be performed independently of visualization tasks.
