function configuration = GetConfig(NbAsics)

    for i = 1:NbAsics

        %% i) Parameters are defined in a txt external file so that they may be changed by the user
        myfile = fullfile(getcurrentdir, 'configuration', ...
                          sprintf('catiroc_parameters_%i.param', i));

        if ~(exist(myfile, 'file') == 2)
            fprintf('\n\tNo %s file found. Program Aborted.\n', myfile)
            return
        else
            fid = fopen(myfile, 'r');
            while ~feof(fid)
                linea = fgetl(fid);
                if isempty(linea),       continue,  end
                if strcmp(linea(1),'#'), continue,  end
                PosEqual      = strfind(linea,'=');
                PosSemiColom  = strfind(linea,';');
                parameter = linea(1:PosEqual-2);
                value     = linea(PosEqual+2:PosSemiColom-1);
                eval(sprintf('configuration(i).%s = ''%s'';', parameter, value))
            end
            fclose(fid);
        end

    end
