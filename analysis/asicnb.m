function asicnb(type, dirname)
% Compute histograming of AsicNb over NbAsics, splitting appart using OddEven

    if nargin==0
        error('At least tell me if I do have to compte phy or dis data ... !')
    elseif nargin==1
        dirname = '.';
    end

    fprintf('\nComputing asic nb analysis ... ')

    if strcmp('phy', type)
        load(fullfile(dirname, './results/decoded_phy.mat'), ...
             'AsicNb', 'EventCounter', 'NbAsics', 'EventKind');
        OddEven = EventCounter;
        AsicNb = AsicNb(EventKind==0);
    elseif strcmp('dis', type)
        load(fullfile(dirname, './results/decoded_dis.mat'), ...
             'AsicNb', 'Edge', 'NbAsics', 'EventKind');
        OddEven = Edge;
        AsicNb = AsicNb(EventKind==2);
    end

    if isempty(AsicNb)
        fprintf('\nNo Channel data found. Aborting.\n')
        return
    end

    HistogramAsicNb = zeros(2, NbAsics, 'uint64');

    index1 = (mod(OddEven, 2) == 1);
    index2 = (mod(OddEven, 2) == 0);

    [n1, ~] = hist(AsicNb(index1), 0:NbAsics-1);
    [n2, ~] = hist(AsicNb(index2), 0:NbAsics-1);

    HistogramAsicNb(1, :) = n1;
    HistogramAsicNb(2, :) = n2;

    HistogramAsicNbFigure = figure('Visible', 'off', ...
                                   'CloseRequestFcn', 'closereq',  ...
                                   'Numbertitle', 'off', ...
                                   'Menubar', 'none', ...
                                   'Toolbar', 'figure', ...
                                   'Name', sprintf('%s', 'Asic Nb' ));

    bar(HistogramAsicNb');
    HistogramAsicNb

    xlabel('AsicNb'), ylabel('Counts')
    title(sprintf('AsicNb Histogram - %s', type))
    zoom on, grid minor, hold on

    set(gca, 'XLim', [0 9])
    set(gca, 'YScale', 'lin')

    if strcmp('phy', type)
        saveas(gcf, fullfile(dirname, './results/asicnb.fig') )
        saveas(gcf, fullfile(dirname, './results/asicnb.png') )
    elseif strcmp('dis', type)
        saveas(gcf, fullfile(dirname, './results/asicnb_discri.fig') )
        saveas(gcf, fullfile(dirname, './results/asicnb_discri.png') )
    end

    close(HistogramAsicNbFigure)

    if strcmp('phy', type)
        save(fullfile(dirname, './results/HistogramAsicNb.mat'), 'HistogramAsicNb')
    elseif strcmp('dis', type)
        save(fullfile(dirname, './results/HistogramAsicNb_discri.mat'), 'HistogramAsicNb')
    end

    fprintf('Done.\n')

end
