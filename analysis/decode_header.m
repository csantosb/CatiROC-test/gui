function AcqRawDataFormatID = decode_header(fid)

    fprintf('Decoding header ... \n\n')

    AcqRawDataFormatID = fread(fid, 1, 'uint8');
    Year               = fread(fid, 1, 'uint16');
    Month              = fread(fid, 1, 'uint8');
    Day                = fread(fid, 1, 'uint8');
    Hour               = fread(fid, 1, 'uint8');
    Minute             = fread(fid, 1, 'uint8');
    Seconds            = fread(fid, 1, 'uint8');
    AsicNb             = fread(fid, 1, 'uint8');
    Command            = fread(fid, 2 + AsicNb*4, 'uint8');
    Configuration      = fread(fid, 2 + AsicNb*66, 'uint8');

    %% Display previous decoded informations

    fprintf('  Date of data taking was ... %i/%i/%i, %i:%i:%i\n', Year, Month, Day, Hour, Minute, Seconds)
    fprintf('  Number of asics ... %i\n', AsicNb)
    fprintf('  AcqRawDataFormatID ... %i\n', AcqRawDataFormatID)

    %% Checkings for data correctness

    %% Command

    if 6 ~= Command(1)
        error('Command doesnt start by %s -> %s', ...
              dec2hex(6), dec2hex(Command(1)))
    end
    if 255 ~= Command(end)
        error('Command doesnt end by %s -> %s', ...
              dec2hex(255), dec2hex(Command(end)))
    end
    fprintf('  OK: Command starts by %s and ends by %s\n', ...
            dec2hex(Command(1)), dec2hex(Command(end)))

    %% Configuration

    if 204 ~= Configuration(1)
        error('Configuration doesnt start by %s -> %s', ...
              dec2hex(204), dec2hex(Configuration(1)))
    end
    if 255 ~= Configuration(end)
        error('Configuration doesnt end by %s -> %s', ...
              dec2hex(255), dec2hex(Command(end)))
    end
    fprintf('  OK: Configuration starts by %s and ends by %s\n', ...
            dec2hex(Configuration(1)), dec2hex(Configuration(end)))

end