function event_counter(type, dirname)
% Check event counter
%
% Plot Diff of event counter by channel
% Should be equal to 1 for a +1 increase

    if nargin==0
        error('At least tell me if I do have to compte phy or dis data ... !')
    elseif nargin==1
        dirname = '.';
    end

    fprintf('\nComputing event counter analysis.\n')

    if strcmp('phy', type)
        load(fullfile(dirname, './results/decoded_phy.mat'), ...
             'Channel', 'EventCounter');
    elseif strcmp('dis', type)
        load(fullfile(dirname, './results/decoded_dis.mat'), ...
             'Channel', 'EventCounter');
    end

    h = figure('Visible', 'off'); hold on, zoom on, grid minor
    xlabel('Bin'), ylabel('Value')
    title('Event Number Diff Histogram, all channels')

    all_channels = unique(Channel)';
    fprintf('\nComputing histogram (between ch. %i and ch. %i) for channel ... ', ...
            all_channels(1), all_channels(end))

    Overall = zeros(21, length(all_channels));

    MinLength = 1e30;

    for i = 1:length(all_channels)

        ch_nb = uint16(all_channels(i));

        fprintf('%i ', ch_nb)

        %% Channel i event counter: extend to 64 bits
        if strcmp('phy', type)
            EventCounter_ch{i} = ...
                convert_overflowing_vector(EventCounter(Channel==ch_nb), 11);
        elseif strcmp('dis', type)
            EventCounter_ch{i} = ...
                convert_overflowing_vector(EventCounter(Channel==ch_nb), 26);
        end

        if length(EventCounter_ch{i}) < MinLength
            MinLength = length(EventCounter_ch{i});
        end

        %% Compute diff
        DiffEventCounter_ch = diff(EventCounter_ch{i});

        %% Print warning about loosing data
        if any(DiffEventCounter_ch>1)
            warning('Diff in eventcounter(channel=%i) > 1 -> loosing data ... be careful\n', i)
        end

        %% Compute histogram
        %% [n, x] = hist(double(DiffEventCounter_ch), 0:double(max(DiffEventCounter_ch)-min(DiffEventCounter_ch)+1));
        [n, x] = hist(double(DiffEventCounter_ch), 0:20);

        Overall(:,i) = n;

    end

    fprintf('\n')

    %% plot
    bar(x, Overall)

    set(gca, 'YScale', 'log')
    set(gca, 'XLim', [0 20])
    tmp = get(gca, 'XLim');
    set(gca, 'XLim', [0 tmp(2)])

    if strcmp('phy', type)
        saveas(h, fullfile(dirname, './results/event_counter_phy.fig'))
        saveas(h, fullfile(dirname, './results/event_counter_phy.png'))
    elseif strcmp('dis', type)
        saveas(h, fullfile(dirname, './results/event_counter_dis.fig'))
        saveas(h, fullfile(dirname, './results/event_counter_dis.png'))
    end

    close(h)

    AllEventCounters = zeros(MinLength, length(all_channels));
    for i = 1:length(all_channels)
        AllEventCounters(:,i) = EventCounter_ch{i}(1:MinLength);
    end

    if strcmp('phy', type)
        save(fullfile(dirname, './results/decoded_phy.mat'), ...
             'AllEventCounters', '-append');
    elseif strcmp('dis', type)
        save(fullfile(dirname, './results/decoded_dis.mat'), ...
             'AllEventCounters', '-append');
    end

end
