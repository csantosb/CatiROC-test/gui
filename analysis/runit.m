function runit(NbAsics, header, dirname)

% * Context

    %% use local dirname by default

    if nargin==2
        dirname = '.';
    end

    %% If no results folder exists, create it

    if 7 ~= exist(fullfile(dirname, 'results'), 'dir')
        mkdir(fullfile(dirname, 'results'))
        fprintf('\nCreating results directory.\n')
    end

% * Decode

    %% When previously decoded data exists, use it instead of decoding raw
    %% data again

    if (2 ~= exist(fullfile(dirname, 'results/decoded_phy.mat'), 'file')) || ...
             (2 ~= exist(fullfile(dirname, 'results/decoded_dis.mat'), 'file'))
        decode(header, dirname, NbAsics);
    else
        fprintf('\nUsing previously decoded data in the results folder.\n')
        fprintf('\nRemove the results folder to force new decoding.\n')
    end

% * Analyse

% ** All data

    fprintf('\n\nRunning all data analysis __________________\n')

    fprintf('\nLoading decoded data.\n')
    load(fullfile(dirname, './results/decoded_phy.mat'), ...
         'AsicNb', 'EventKind');

    AsicNb_ = unique(AsicNb);
    fprintf('\nFound data corresponding to asics ... ')
    for i = 1:length(AsicNb_)
        fprintf('  %i  ', AsicNb_(i))
    end
    fprintf('\n')

% ** Physical data

    fprintf('\n\nRunning physical data analysis __________________\n')

    CheckDataFound(AsicNb, EventKind, 0)

    charge(dirname)

    coarse_time('phy', dirname)
    event_counter('phy', dirname)
    fine_time('phy', dirname)
    if NbAsics>1
        asicnb('phy', dirname)
    end
    channelnb('phy', dirname)

% ** Discriminator data

    fprintf('\n\nRunning discriminator data analysis __________________\n')

    CheckDataFound(AsicNb, EventKind, 2)

    coarse_time('dis', dirname)
    event_counter('dis', dirname)
    fine_time('dis', dirname)
    if NbAsics>1
      asicnb('dis', dirname)
    end
    channelnb('dis', dirname)
    if NbAsics>1
      tdc(dirname)
    end

end

% * CheckDataFound

function CheckDataFound(AsicNb, EventKind, KindOfEvent)

    AsicNb_ = unique(AsicNb(EventKind==KindOfEvent));
    if isempty(AsicNb_)
        fprintf('\nFound no data.\n')
        return
    end

    fprintf('\nFound data corresponding to asics ... ')
    for i = 1:length(AsicNb_)
        fprintf('  %i  ', AsicNb_(i))
    end
    fprintf('\n')

end