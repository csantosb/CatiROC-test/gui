function config = GetGlobalParameters()

% Parameters are defined in a txt external file so that they may be changed by the user
myfile = 'global_parameters.param';

if ~(exist(sprintf('%c%s', getcurrentdir, filesep, myfile), 'file') == 2)
    fprintf('\n\tNo %s file found. Program Aborted.\n', myfile)
    return
else
    fid = fopen(sprintf('configuration%s%s', filesep, myfile),'r');
    while ~feof(fid)
        linea = fgetl(fid);
        if isempty(linea),       continue,  end
        if strcmp(linea(1),'#'), continue,  end
        PosEqual      = strfind(linea,'=');
        PosSemiColom  = strfind(linea,';');
        parameter = linea(1:PosEqual-2);
        value     = linea(PosEqual+2:PosSemiColom-1);
        eval(sprintf('config.%s = %s;', parameter, value))
    end
    fclose(fid);
end