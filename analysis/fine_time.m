function fine_time(type, dirname)
% Compute fine time histograms.
%
% Bla ...

    if nargin==0
        error('At least tell me if I do have to compte phy or dis data ... !')
    elseif nargin==1
        dirname = '.';
    end

    fprintf('\nComputing fine time analysis.\n')

    if strcmp('phy', type)
        NbOfBits = 10;
        load(fullfile(dirname, './results/decoded_phy.mat'), ...
             'Channel', 'FineTime', 'EventCounter', 'NbAsics');
    elseif strcmp('dis', type)
        NbOfBits = 6;
        load(fullfile(dirname, './results/decoded_dis.mat'), ...
             'Channel', 'FineTime', 'Edge', 'NbAsics');
        EventCounter = Edge;
    end

    if Channel == -1
        fprintf('\nNo Channel data found. Aborting.\n')
        return
    end

    %% Preallocate arrays
    HistogramADCTime   = zeros(2^NbOfBits, 16*2*NbAsics, 'uint64');

    %% Figure to hold histograms
    HistogramADCTimeFigure = figure('Visible', 'off', ...
                                    'CloseRequestFcn', 'closereq',  ...
                                    'Numbertitle', 'off', ...
                                    'Menubar', 'none', ...
                                    'Toolbar', 'figure', ...
                                    'Name', sprintf('%s', 'ADC Fine Time' ));

    HistogramADCTimeFigureAxis = gca;
    PlotHandleHistogramADCTime = plot(HistogramADCTime);
    SetColor(PlotHandleHistogramADCTime)
    xlabel('Channel'), ylabel('Counts'), title('Fine Time Histogram')
    zoom on, grid minor, hold on
    if NbAsics==1
        legend('0','0','1','1','2','2','3','3','4','4','5','5','6','6','7','7','8','8',...
               '9','9','10','10','11','11','12','12','13','13', ...
               '14','14','15','15','Location', ...
               'eastoutside')
    end

    %% Figure to hold histograms
    HistogramADCTimeFigure_offset = figure('Visible', 'off', ...
                                           'CloseRequestFcn', 'closereq',  ...
                                           'Numbertitle', 'off', ...
                                           'Menubar', 'none', ...
                                           'Toolbar', 'figure', ...
                                           'Name', sprintf('%s', 'ADC Fine Time' ));

    HistogramADCTimeFigureAxis_offset = gca;
    PlotHandleHistogramADCTime_offset = plot(HistogramADCTime);
    SetColor(PlotHandleHistogramADCTime_offset)
    xlabel('Channel'), ylabel('Counts'), title('Fine Time Histogram')
    zoom on, grid minor, hold on
    if NbAsics==1
        legend('0','0','1','1','2','2','3','3','4','4','5','5','6','6','7','7','8','8',...
               '9','9','10','10','11','11','12','12','13','13', ...
               '14','14','15','15','Location', ...
               'eastoutside')
    end

    all_channels = unique(Channel)';

    fprintf('\nComputing histogram (between ch. %i and ch. %i) for channel ... ', ...
            all_channels(1), all_channels(end))

    MinLength = 1e30;

    for i = 1:length(all_channels)

        ch_nb = uint16(all_channels(i));

        fprintf('%i ', ch_nb)

        tmp = (Channel == ch_nb);
        if strcmp('phy', type)
            tmp2 = bitget(EventCounter, 1);
        elseif strcmp('dis', type)
            tmp2 = EventCounter;
        end
        index1 = tmp & tmp2;
        index2 = tmp & ~tmp2;

        FineTime_ch{i} = FineTime(tmp);

        if length(FineTime_ch{i}) < MinLength
            MinLength = length(FineTime_ch{i});
        end

        HistogramADCTime(:, 2*ch_nb+1) = HistogramADCTime(:, 2*ch_nb+1) + ...
            uint64(hist(FineTime(index1), 0:(2^NbOfBits-1)))';

        HistogramADCTime(:, 2*ch_nb+2) = HistogramADCTime(:, 2*ch_nb+2) + ...
            uint64(hist(FineTime(index2), 0:(2^NbOfBits-1)))';

        set(PlotHandleHistogramADCTime(2*ch_nb+1), 'YData', HistogramADCTime(:, 2*ch_nb+1))
        set(PlotHandleHistogramADCTime(2*ch_nb+2), 'YData', HistogramADCTime(:, 2*ch_nb+2))

        set(PlotHandleHistogramADCTime_offset(2*ch_nb+1), 'YData', uint64(i*1e2) + HistogramADCTime(:, 2*ch_nb+1))
        set(PlotHandleHistogramADCTime_offset(2*ch_nb+2), 'YData', uint64(i*1e2) + HistogramADCTime(:, 2*ch_nb+2))

    end % for channels

    fprintf('\n')

    set(HistogramADCTimeFigureAxis, 'XLim', [0 2^NbOfBits-1])
    set(HistogramADCTimeFigureAxis, 'YScale', 'lin')

    set(HistogramADCTimeFigureAxis_offset, 'XLim', [0 2^NbOfBits-1])
    set(HistogramADCTimeFigureAxis_offset, 'YScale', 'lin')

    if strcmp('phy', type)
        saveas(HistogramADCTimeFigure, fullfile(dirname, './results/fine_time.fig'))
        saveas(HistogramADCTimeFigure, fullfile(dirname, './results/fine_time.png'))
    elseif strcmp('dis', type)
        saveas(HistogramADCTimeFigure, fullfile(dirname, './results/fine_time_discri.fig'))
        saveas(HistogramADCTimeFigure, fullfile(dirname, './results/fine_time_discri.png'))
    end

    if strcmp('phy', type)
        saveas(HistogramADCTimeFigure_offset, fullfile(dirname, './results/fine_time_offset.fig'))
        saveas(HistogramADCTimeFigure_offset, fullfile(dirname, './results/fine_time_offset.png'))
    elseif strcmp('dis', type)
        saveas(HistogramADCTimeFigure_offset, fullfile(dirname, './results/fine_time_discri_offset.fig'))
        saveas(HistogramADCTimeFigure_offset, fullfile(dirname, './results/fine_time_discri_offset.png'))
    end

    close(HistogramADCTimeFigure)
    close(HistogramADCTimeFigure_offset)

    %% Save data
    if strcmp('phy', type)
        save(fullfile(dirname, './results/HistogramADCTime.mat'), 'HistogramADCTime')
    elseif strcmp('dis', type)
        save(fullfile(dirname, './results/HistogramADCTime_discri.mat'), 'HistogramADCTime')
    end

    %% Bidim
    if strcmp('phy', type)
        data = HistogramADCTime(:,:);
    elseif strcmp('dis', type)
        data = HistogramADCTime(1:24,:);
    end

    data(data==0) = mean(data(data>0));
    ref =  2*mean(data(data>5));

    data(data>ref) = ref;
    [x,y]= meshgrid(1:size(data,1), 1:size(data,2));
    hh = figure('Visible', 'off');
    surf(y', x', data), view(2), shading interp, axis tight
    colorbar, colormap hot

    if strcmp('phy', type)
        saveas(hh, fullfile(dirname, './results/fine_time_bidim.fig'))
        saveas(hh, fullfile(dirname, './results/fine_time_bidim.png'))
    elseif strcmp('dis', type)
        saveas(hh, fullfile(dirname, './results/fine_time_discri_bidim.fig'))
        saveas(hh, fullfile(dirname, './results/fine_time_discri_bidim.png'))
    end

    close(hh)

    AllFineTimes = zeros(MinLength, length(all_channels));
    for i = 1:length(all_channels)
        AllFineTimes(:,i) = FineTime_ch{i}(1:MinLength);
    end

    if strcmp('phy', type)
        save(fullfile(dirname, './results/decoded_phy.mat'), ...
             'AllFineTimes', '-append');
    elseif strcmp('dis', type)
        save(fullfile(dirname, './results/decoded_dis.mat'), ...
             'AllFineTimes', '-append');
    end

end