function obj = GetSCurve(obj, handles)
% GETSCURVE - Updates configuration of ADC through SPI

%   GETSCURVE -
%   sends
%

%% Return if handle not open
if not(GetIsOpen(obj))

    error('GETSCURVE: Device not open.')

end

%% Update contents of global params config file
obj.GlobalParameters = GetGlobalParameters;

%% Amount of bytes to request

%% = nb_times * bytes * channels
AmountInRxQueue = obj.GlobalParameters.s_curve.nb_periods*4*16*obj.nb_asics;
AmountOfEmptyDataPacketsReceived = 0;

%% Init buffers to allocate memory
lpBufferPtr_big = libpointer('voidPtr', zeros(AmountInRxQueue, 1, 'uint8'));
el_read_ptr     = libpointer('ulongPtr',uint32(0));

%% From config file: limits to scan
limits = obj.GlobalParameters.s_curve.th.min:...
         obj.GlobalParameters.s_curve.th.step:...
         obj.GlobalParameters.s_curve.th.max;

%% Initialize arrays
SCurves = zeros(length(limits), 16*obj.nb_asics);

TotalStats = zeros(length(limits), 16*obj.nb_asics);

%% Create new figure
FigureHandle = figure('Visible', 'on');
hold on, grid minor, zoom on
xlabel('Threshold'), title('S Curve')
if obj.GlobalParameters.s_curve.method
    ylabel('# events - using data')
else
    ylabel('# events - using triggers')
end

%% Plot data and store handle to refresh afterwards
% FigureHandlePlot = plot(limits, SCurves, '*-');
FigureHandlePlot = plot(limits, zeros(length(limits), 16*obj.nb_asics), '*-');
% FigureHandlePlot2 = plot(limits, SCurves(:,9:end), '*--');
% FigureHandlePlot = [FigureHandlePlot1; FigureHandlePlot2];
%     legend('Location','eastoutside', ...
%            '1','2','3','4','5','6','7','8','9', ...
%            '10','11','12','13','14','15','16')
% legend

%% Config ASIC with updated threshold
obj = obj.UpdateConfig;

%% Fix values to override in configuration of ASIC
for i = 1:obj.nb_asics
    obj.configuration(i).Discri.TimeDAC.format = 'dec';
% obj.configuration.Discri.ChargeDAC.format = 'dec';
% obj.configuration.Discri.ChargeDAC.value = '500';
    obj.configuration(i).Discri.TimeDAC.value = num2str(limits(1));
end

[obj, ConfigureASICWrong] = obj.ConfigureASIC;

iConfigureASICWrong = 1;
if ConfigureASICWrong
    while (ConfigureASICWrong && iConfigureASICWrong < 5)
        fprintf('\n\tRetrying ConfigureASIC, retry nb %i:\n', ...
                iConfigureASICWrong)
        [obj, ConfigureASICWrong] = obj.ConfigureASIC;
        iConfigureASICWrong = iConfigureASICWrong + 1;
    end
end

%% Command to start acquisition
Command = ([9, ...
            0, ...
            obj.GlobalParameters.s_curve.method, ... % use triggers or events
            0, ... % bitand(bitshift(aa,-8), 255), ... % bits 15 to 8
            0, ... % bitand(aa,255), ... % rightmost 8 bits, 7 to 0
            255]);

%% Let’s go
i = 1;

while (i < length(limits)+1 ) && get(gcbo,'Value')

    %% Print infos
    fprintf('\n\n\n\tThreshold :: %2i _____________________\n\n', limits(i))

    %% Erase any data around in usb bus
    obj.PurgeUSB

    %% Config ASIC with updated threshold
    for ii = 1:obj.nb_asics
%         obj.configuration(ii).Mask.format = 'hex';
%         obj.configuration(ii).Mask.value = 'FFFF';
        obj.configuration(ii).Discri.TimeDAC.format = 'dec';
        obj.configuration(ii).Discri.TimeDAC.value = num2str(limits(i));
    end
    [obj, ConfigureASICWrong] = obj.ConfigureASIC;
    iConfigureASICWrong = 1;
    if ConfigureASICWrong
        while (ConfigureASICWrong && iConfigureASICWrong < 5)
            fprintf('\n\tRetrying ConfigureASIC, retry nb %i:\n', ...
                    iConfigureASICWrong)
            [obj, ConfigureASICWrong] = obj.ConfigureASIC;
            iConfigureASICWrong = iConfigureASICWrong + 1;
        end
    end

    %% Send command "09" to request data
    %% Start processing statistics onboard
    obj.SendCommand(Command);

    while (obj.GetStatusUSB < AmountInRxQueue)
        pause(.1)
    end

    %% Read Data. Time out is 3 s.
    [status, ~, el_read] = calllib('libftdi', 'ReadUSB', ...
                                   obj.DevNumber, ...
                                   lpBufferPtr_big, ...
                                   AmountInRxQueue, ...
                                   el_read_ptr);

    %% ReadUSB returns ~= 0 if el_read ~= AmountInRxQueue
    if AmountOfEmptyDataPacketsReceived == 3 && status==50  % No data at all: stop
        fprintf('\n\tError: No data !!. \n\nStopped.\n'),
        return
    elseif status==50  % No data at all
        fprintf('\n\tError: No data !!\n'),
        AmountOfEmptyDataPacketsReceived = AmountOfEmptyDataPacketsReceived + 1;
        drawnow
        continue
    elseif status ~= 0 % low data
        fprintf('\n\tWarning: %s, bytes read is %i; ignoring data\n', obj.status_chain(status), el_read),
        drawnow
        continue
    end
    AmountOfEmptyDataPacketsReceived = 0;

    %% Start processing data

    %% Convert to 4 columns
    data = double(reshape(lpBufferPtr_big.Value, 4, []))';

    %% Convert to 32 bits
    data = data(:,1)*2^24 + data(:,2)*2^16 + data(:,3)*2^8 + data(:,4);

    %% Asic number
    AsicNb = bitshift(data, -29);

    %% Channel number
    ChNb = bitand(bitshift(data, -25), (2^4-1));

    %% Value
    Value = bitand(data, 2^25-1);

    %% Check validity of channel numbers, and its ordering
%     myref = reshape(flipud(reshape(obj.GlobalParameters.s_curve.nb_periods*(0:1:15), 4,[])),1,[])';
%     if not(isequal(sum(reshape(ChNb, 16, []), 2), ...
%                    myref))
%             fprintf('\n\tProblem at threshold %3i, restarting ... \n', ...
%                     limits(i))
%             continue
%     end

    for ii = 0:obj.nb_asics-1
        for jj = 0:15
            ComputedValues = Value(AsicNb==ii & ChNb==jj);
            if ~isempty(ComputedValues)
                SCurves(i, ii*16+jj+1) = mean(ComputedValues);
                TotalStats(i, ii*16+jj+1) = length(ComputedValues);
            end
        end
    end

%   Value=Value(17:end);
%   Value = mean(reshape(Value, 16, []), 2);

%     SCurves(i,:) = Value;

%     fprintf('\nSCurves (1e3) :\n\n')
%     fprintf(['%6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f ' ...
%              '%6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f\n'], SCurves'/1000)
%
%     fprintf('\nTotalStats :\n\n')
%     fprintf(['%6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f ' ...
%              '%6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f\n'], TotalStats')

    %% Update plots
    for k = 1:16*obj.nb_asics
      set(FigureHandlePlot(k), 'YData', SCurves(:, k))
    end

    drawnow

    i = i + 1;

end

%% Get save data flag from gui
SaveDataFlag = get(handles.save_data_tag, 'Value');

%% Create a random folder where to save data
if SaveDataFlag

    %% open file
    myrandom   = dec2hex(fix(mod(rand*now, 65530)), 4);
    mydir      = [getcurrentdir filesep sprintf('s_curve_data_%s.bin', myrandom)];
    if ~ (exist(mydir, 'dir') == 7)
        mkdir(mydir)
    end
    fprintf('\n\tSaving data to %s\n', mydir)

    %% copy config files to data saving dir
    for i = 1:obj.nb_asics
        copyfile(fullfile('configuration', sprintf('catiroc_parameters_%i.param', i)), ...
                 fullfile(mydir))
    end
    copyfile(fullfile('configuration', 'global_parameters.param'), ...
             fullfile(mydir))

end

%% Save computed s_curve to disk
if SaveDataFlag

    myfile = 's_curves.txt';
    fprintf('\n\tSaving s curves to file %s.\n', myfile)
    dlmwrite(fullfile(mydir, myfile), SCurves, 'delimiter' ,'\t', ...
             'precision',3)

    %% Save figure too
    saveas(FigureHandle, fullfile(mydir, 's_curves.fig'))
    saveas(FigureHandle, fullfile(mydir, 's_curves.png'))
    saveas(FigureHandle, fullfile(mydir, 's_curves.jpg'))

end

set(gcbo,'Value', 0)
fprintf('\n\tDone.\n')
