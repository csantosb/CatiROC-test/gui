function el_written = WriteRandomRB(obj, length)

    if GetIsOpen(obj)

        InitData   = uint8([5;250*rand(length-2,1);255]);
        el_written = obj.WriteUSB(InitData);
        fprintf('\n\tBytes written %i\n', el_written)
        InitData'

    else

        error('SENDCOMMAND: Device not open.')

    end