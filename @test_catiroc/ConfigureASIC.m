function [obj, ConfigureASICWrong] = ConfigureASIC(obj)
% CONFIGUREASIC - Update configuration and send to ASIC

%   CONFIGUREASIC - Reads configuration file, updating object properties and
%   sends it to the ASIC. Then, reads it back from ASIC, to check correct
%   configuration process

if obj.GetIsOpen

    ConfigureASICWrong = 0;

    %% First, update the configuration_array with infos in configuration.structure

    for i = 1:obj.nb_asics

        %% iii) Init and update byte array
        %% conf_bin_array is a binary representation of the
        %% configuration file
        conf_bin_array = repmat('0', 1, (328+200)); % Init array

        %% Slow Control Registers

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).EnableInputTestChannel, 0, 16, 1, 0);

        conf_bin_array = RegisterToConfigArray_value_ext(conf_bin_array, obj.configuration(i).sw_cf, 16, 8, 16, 1);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.Preamp.HG, 176);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.Preamp.LG, 178);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).SlowShapper.PowerOn.HG, 180);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SlowShapper.Gain.HG, 182, 2, 1, 1);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SlowShapper.TimeConstant.HG, 184, 3, 1, 1);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).SlowShapper.PowerOn.LG, 187);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SlowShapper.Gain.LG, 189, 2, 1, 1);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SlowShapper.TimeConstant.LG, 191, 3, 1, 1);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SlowShapper.Feedback.HG, 194, 3, 1, 1);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SlowShapper.Feedback.LG, 197, 3, 1, 1);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).SlowShapper.PowerOnSCA, 200);

        conf_bin_array(202 + 1) = obj.configuration(i).ModeSelection.Gain;

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.GainDiscri, 203);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).FastShaper.PowerOn, 205);

        conf_bin_array(207 + 1) = obj.configuration(i).ModeSelection.Hold;

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.TimeDiscri, 208);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.TDC, 210);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.ChargeADC, 212);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.TimeADC, 214);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SelData, 216, 2, 1, 0);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.DACDelay, 218);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).Delay, 220, 8, 1, 1);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).Mask, 228, 16, 1, 0);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SlowShapper.Sent, 244, 2, 16, 0);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.DAC, 276);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).Discri.TimeDAC, 278, 10, 1, 1);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).Discri.ChargeDAC, 288, 10, 1, 1);

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.RampChargeADC, 298);

        conf_bin_array(300 + 1) = obj.configuration(i).AddCompensatedSwitches.ChargeADCRamp;

        conf_bin_array(301 + 1) = obj.configuration(i).ChargeADCRampSlope;

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.RampTimeADC, 302);

        conf_bin_array(304 + 1) = obj.configuration(i).AddCompensatedSwitches.TimeADCRamp;

        conf_bin_array(305 + 1) = obj.configuration(i).TimeADCRampSlope;

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.OTA, 306);

        conf_bin_array(308 + 1) = obj.configuration(i).SwitchOff.OTA;

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.SlowLVDSReceiver, 309);

        conf_bin_array(311 + 1) = obj.configuration(i).SwitchOff.LVDSReceiver40MHz;

        conf_bin_array(312 + 1) = obj.configuration(i).SwitchOff.LVDSReceiver160MHz;

        conf_bin_array(313 + 1) = obj.configuration(i).SelClkDiv4;

        conf_bin_array(314 + 1) = obj.configuration(i).Sel80M;

        conf_bin_array(315 + 1) = obj.configuration(i).DisableOvfCpt;

        conf_bin_array(316 + 1) = obj.configuration(i).SelExtRazChannel;

% obj.conf_bin_array(317) = ;

        conf_bin_array(318 + 1) = obj.configuration(i).SelExtRead;

        conf_bin_array(319 + 1) = obj.configuration(i).EnableTacReadout;

        conf_bin_array(320 + 1) = obj.configuration(i).EnableNOR16;

        conf_bin_array(321 + 1) = obj.configuration(i).EnableTransmit;

        conf_bin_array(322 + 1) = obj.configuration(i).EnableData_oc;

        conf_bin_array(323 + 1) = obj.configuration(i).DisableTriggerBuffer;

        conf_bin_array = RegisterToConfigArray_en_pp(conf_bin_array, obj.configuration(i).PowerOn.LVDSTransceiver, 324);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).SwitchTxBias, 326, 2, 1, 0);

        %% Probe Registers

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).probe.data, 328+0,   32, 1, 0);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).probe.dig2, 328+32,  32, 1, 0);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).probe.fsh,  328+64,  16, 1, 0);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).probe.dig1, 328+80,  48, 1, 0);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).probe.ssh,  328+128, 32, 1, 0);

        conf_bin_array = RegisterToConfigArray_value(conf_bin_array, obj.configuration(i).probe.pa,   328+160, 32, 1, 0);

        conf_bin_array(328 + 192 + 1) = obj.configuration(i).probe.dig0;

        conf_bin_array(328 + 193 + 1) = obj.configuration(i).probe.dig4;


        %% Reshaping

        %% Reshape first 328 bits to 41 bytes
        conf_bin_array_slow_control = reshape(conf_bin_array(328:-1:1), 8, 41)';
        if obj.GlobalParameters.verbose
            fprintf('\n\t Slow Control Register, asic %i:\n', i)
            SLOW_CONTROL_REGISTER = reshape(conf_bin_array(1:328),8,[])';
            for j = 1:41
                fprintf('\n\t\t\t Bit %3i to %3i  ->  %s, reg. %3i', ...
                        (j-1)*8+7, ...
                        (j-1)*8, ...
                        fliplr(SLOW_CONTROL_REGISTER(j,:)), ...
                        bin2dec(fliplr(SLOW_CONTROL_REGISTER(j,:))));
            end
            fprintf('\n\n')
        end

        %% Reshape remaining 200 bits to 25 bytes
        conf_bin_array_probe = ...
            reshape([conf_bin_array(end-6:-1:329), repmat('0', 1, 6)], 8, 25)';

        %% Print some infos for debugging
        if obj.GlobalParameters.verbose
            fprintf('\n\t Probe Register, asic %i:\n', i)
            PROBE_REGISTER = reshape(conf_bin_array(329:end),8,[])';
            for j = 1:25
                fprintf('\n\t\t\t Bit %3i to %3i  ->  %s, reg. %3i', ...
                        (j-1)*8+7, ...
                        (j-1)*8, ...
                        fliplr(PROBE_REGISTER(j,:)), ...
                        bin2dec(fliplr(PROBE_REGISTER(j,:))));
            end
            fprintf('\n\n')
        end

        %% Save config to disk
        if obj.GlobalParameters.verbose
            myfile = 'last_configuration_sent.txt';
            fprintf('\n\tSaving configuration to file %s.\n', myfile)
            tmp = reshape(conf_bin_array_slow_control', 1, []);
            fid = fopen(myfile, 'w');
            for j = 0:length(tmp)-1
                fprintf(fid, '%i\n', str2num(tmp(length(tmp)-j)));
            end
            fclose(fid);
        end

        %% Cat slow control and probe registers and add them to configuration_array
        tmp = bin2dec([conf_bin_array_slow_control ; ...
                       conf_bin_array_probe]);
        indices = (i-1)*66 + (2:2+65);
%             tmp = fix(rand(66,1)*255);
%             tmp(end) = bitand(tmp(end),bin2dec('11000000'));
        obj.configuration_array(indices) = tmp;


    end % for i = 1 : nb_asics

    %% Erase any data around in usb bus
    obj.PurgeUSB

    %% Following contents of the config file,
    %%     # [#313] - Sel external (0) or internal (1) 40MHz, 1 bit
    %%     # int : 160MHz/4, ext : LVDS RX
    %% disable sending the 40MHz clock to ASIC
    %% Command "10" is used for this (see firmware)
    if str2double(obj.configuration(1).SelClkDiv4)
        %% internal clock
        %% command "0A"
        obj.SendCommand([10,1,255]);
        fprintf('\n\tDisabled sending 40 MHz clock.\n')
    else
        %% external clock
        %% command "0A"
        obj.SendCommand([10,0,255]);
        fprintf('\n\tEnabled sending 40 MHz clock.\n')
    end

    %% Send configuration to card
    obj.SendCommand(obj.configuration_array);

    pause(.5)

    %% Check if config is available in return
    AmountOfDataAvailable = obj.GetStatusUSB;
    ToCompare_tmp = 66*3*obj.nb_asics;
    if AmountOfDataAvailable ~= ToCompare_tmp

        warning('CONFIGUREASIC: No configuration available in return, only %i words, retrying ...', ...
                AmountOfDataAvailable)

        ConfigureASICWrong = 1;

        return

%         %% Erase any data around in usb bus
%         obj.PurgeUSB
%
%         %% Send configuration to card
%         obj.SendCommand(obj.configuration_array);
%
%         %% Check if config is available in return
%         AmountOfDataAvailable = obj.GetStatusUSB;
%
%         if AmountOfDataAvailable ~= 66*3*obj.nb_asics
%             error('CONFIGUREASIC: No configuration available in return, only %i words, retrying ...', ...
%                   AmountOfDataAvailable)
%         end

    end

    %% Read it back
    [AmountBytesRead, BytesRead] = obj.ReadUSB;

    %% Check data returned
    ToCompare_tmp = 66*3*obj.nb_asics;
    if AmountBytesRead ~= ToCompare_tmp
        error('CONFIGUREASIC: No configuration returned')
    end

    %% Rearrange BytesRead
    BytesRead = reshape(BytesRead, 3, [])';
    for i=1:obj.nb_asics
        BytesRead_ch(:,i) = BytesRead((BytesRead(:,2)==(i-1)));
    end

    %BytesRead_ch = fliplr(BytesRead_ch);

    for i=1:obj.nb_asics
        %% Check config sent is equal to config read
        indices = (i-1)*66 + [2:2+65];
        % obj.configuration_array(indices)
        if ~ isequal(BytesRead_ch(:,i), obj.configuration_array(indices)')
            warning('\nCONFIGUREASIC: Configuration returned not equal to sent ASIC %i !!', i)
            find(BytesRead_ch(:,i) ~= obj.configuration_array(indices)')'
            if obj.GlobalParameters.verbose
                BytesRead,
            end
            ConfigureASICWrong = 1;
            return
        else
            fprintf('\n\tConfiguration ASIC %i OK.\n', i)
        end
    end

    %% Following the status of the configuration file, if timing information
    %% is not requested, disable corresponding check box in gui
%     if not(str2double(obj.configuration(1).EnableTacReadout))
%         fprintf('\n\tNo timing information requested.\n')
%         set(handles.adc_time_tag, 'Enable', 'off')
%     else
%         fprintf('\n\tTiming information requested.\n')
%         set(handles.adc_time_tag, 'Enable', 'on')
%     end

else

    error('CONFIGUREASIC: Device not open.')

end