function  [lpdwAmountInRxQueue, lpdwAmountInTxQueue, lpdwEventStatus] = GetStatusUSB(obj)
% GETSTATUSUSB - Get the number of bytes ready to be read in usb

    if GetIsOpen(obj)

        if strcmp('win64',computer('arch'))
            lpdwAmountInRxQueuePtr  = libpointer('ulongPtr',uint32(0));
            lpdwAmountInTxQueuePtr  = libpointer('ulongPtr',uint32(0));
            lpdwEventStatusPtr      = libpointer('ulongPtr',uint32(0));
        else
            lpdwAmountInRxQueuePtr  = libpointer(obj.pointer_type,uint32(0));
            lpdwAmountInTxQueuePtr  = libpointer(obj.pointer_type,uint32(0));
            lpdwEventStatusPtr      = libpointer(obj.pointer_type,uint32(0));
        end

        ft_status = calllib('libftdi', 'GetRxTxStatusUSB', ...
                            obj.DevNumber, ...
                            lpdwAmountInRxQueuePtr,...
                            lpdwAmountInTxQueuePtr, ...
                            lpdwEventStatusPtr);

        lpdwAmountInRxQueue     = get(lpdwAmountInRxQueuePtr, 'Value');
        lpdwAmountInTxQueue     = get(lpdwAmountInTxQueuePtr, 'Value');
        lpdwEventStatus         = get(lpdwEventStatusPtr, 'Value');

        if obj.GlobalParameters.verbose
            fprintf('\n\tUpdate Status USB device %i ... %s\n', ...
                    obj.DevNumber, ...
                    obj.status_chain(ft_status))
            fprintf('\n\t%i bytes in Rx Queue, %i bytes in Tx Queue, %i EventStatus\n', ...
                    lpdwAmountInRxQueue, ...
                    lpdwAmountInTxQueue, ...
                    lpdwEventStatus)
        end

    else

        disp('no handle exists.')

    end

end