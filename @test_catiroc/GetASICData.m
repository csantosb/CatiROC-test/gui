function GetASICData(obj, emulate, handles)
% GEASICTDATA - Read data from ASIC and display in terminal

%   GETASICDATA - Obtains a packet of data from the ASIC and prints first TODISPLAY events
%
%   GEASICTDATA( EMULATE, FRAMEMODE, DECODE) obtains the data following
%   next indications
%   EMULATE - don't take real data, but emulated; useful for debugging
% TODISPLAY - print information regarding this number of events by received data packet

if not(GetIsOpen(obj))

    error('GEASICTDATA: Device not open.')

end

%% Update contents of global params config file
obj.GlobalParameters = GetGlobalParameters;

MainGuiFigureHandle = get(get(gcbo, 'Parent'), 'Parent');

% Parse configuration file
% updates "configuration" object field
obj = obj.UpdateConfig;

% Send config to asic and check
% updates "configuration_array" object field
[obj, ConfigureASICWrong] = obj.ConfigureASIC;
iConfigureASICWrong = 1;
if ConfigureASICWrong
    while (ConfigureASICWrong && iConfigureASICWrong < 5)
        fprintf('\n\tRetrying ConfigureASIC, retry nb %i:\n', ...
                iConfigureASICWrong)
        [obj, ConfigureASICWrong] = obj.ConfigureASIC;
        iConfigureASICWrong = iConfigureASICWrong + 1;
    end
end

NbEventsByChannel = zeros(16, 1);

TriggerRate = zeros(1, 16);
% FirstTime = 1;

%% Compute how many events will be ignored at the beginning of the
%% acquisition. These events originate from the ASIC (as much as we know) and
%% must be removed propertly.
if isfield(obj.GlobalParameters, "IgnoreFakeEvents") && ...
        obj.GlobalParameters.IgnoreFakeEvents
    %% Assumes that there is one fake event by half asic (group of 8
    %% channels). One needs to stimate how many of them to ignore from the
    %% config files
    NbEventsToIgnore = 0;
    %% for each asic
    for i = 1:obj.nb_asics
        %% if asic enabled
        if bitget(hex2dec(obj.GlobalParameters.EnabledASICs), i)
            %% first 8 channels
            if ~(255 == hex2dec(obj.configuration(i).Mask.value(1:2)))
                NbEventsToIgnore = NbEventsToIgnore + 1;
            end
            %% remaining 8 channels
            if ~(255 == hex2dec(obj.configuration(i).Mask.value(3:4)))
                NbEventsToIgnore = NbEventsToIgnore + 1;
            end
        end
    end
    fprintf('\n\t%i Fake events from beginning of acquisition will be ignored.\n', NbEventsToIgnore)
    fprintf('\n\tYou may disable this feature in the global configuration file.\n')
end

%% Save data to disk flag
SaveDataFlag = logical(get(handles.save_data_tag, 'Value'));

%% Save data to disk and compute histograms flag
SaveAndDisplay = logical(obj.GlobalParameters.SaveAndDisplay);

%% Warning about risk of loosing data when saving and displaying
if SaveDataFlag && SaveAndDisplay
    fprintf('\n\t-------------------------------------\n')
    fprintf('\n\tWARNING: Saving data and computing histograms:\n')
    fprintf('\n\tWARNING: this might produce some data lose !!!\n\n')
    fprintf('\n\tWARNING: You can disable computing while saving by disabling \n')
    fprintf('\n\tWARNING: SaveAndDisplay in global_parameters.param\n')
    fprintf('\n\t-------------------------------------\n')
end

todisplay = obj.GlobalParameters.ToDisplay;
tic
% if todisplay, tic, end
% Randomize = handles.GlobalParameters.Randomize;
% convert incoming data from gray to unsigned

AmountInRxQueue = obj.GlobalParameters.AmountInRxQueue;
AmountOfEmptyDataPacketsReceived = 0;

% * Figures to hold plots

if ~SaveDataFlag || (SaveDataFlag && SaveAndDisplay)

    HistogramADCCharge = zeros(2^10, 16*2*obj.nb_asics, 'uint64');
    HistogramADCTime   = zeros(2^10, 16*2*obj.nb_asics, 'uint64');

    HistogramADCChargeFigure = figure('Visible', 'off', ...
                                      'CloseRequestFcn', ' ',  ...
                                      'Numbertitle', 'off', ...
                                      'Menubar', 'none', ...
                                      'Toolbar', 'figure', ...
                                      'Name', sprintf('%s', 'ADC Charge' ));

    hold on
    HistogramADCChargeFigureAxis = gca;
    set(HistogramADCChargeFigureAxis, 'XLim', [1 1024])
    PlotHandleHistogramADCCharge = plot(HistogramADCCharge);
    if obj.nb_asics==1
        obj.SetColor(PlotHandleHistogramADCCharge)
    end
    xlabel('Channel'), ylabel('Counts'), zoom on, grid minor
    if obj.nb_asics==1
        obj.SetFigure
    end

    if str2double(obj.configuration(1).EnableTacReadout)

        HistogramADCTimeFigure = figure('Visible', 'off', ...
                                        'CloseRequestFcn', ' ',  ...
                                        'Numbertitle', 'off', ...
                                        'Menubar', 'none', ...
                                        'Toolbar', 'figure', ...
                                        'Name', sprintf('%s', 'ADC Fine Time' ));

        hold on
        HistogramADCTimeFigureAxis = gca;
        set(HistogramADCTimeFigureAxis, 'XLim', [1 1024])
        PlotHandleHistogramADCTime = plot(HistogramADCTime);
        if obj.nb_asics==1
            obj.SetColor(PlotHandleHistogramADCTime)
        end
        xlabel('Channel'), ylabel('Counts'), zoom on, grid minor
        if obj.nb_asics==1
            obj.SetFigure
        end

    end

end

% * Command to trigger acquisition

ForceTrigger = get(handles.force_trigger_tag, 'Value');
ForceTriggerValue = obj.GlobalParameters.ForceTrigRate;

%% Command to activate peripheral 2 (asic data taking)

Command = uint8([6, ... % order
                 repmat([...
                     hex2dec(obj.GlobalParameters.EnabledASICs), ...
                     obj.GlobalParameters.BoardId, ...
                     ForceTriggerValue*4 + ...
                     obj.GlobalParameters.EnableExternalTrigger*2 + ...
                     ForceTrigger, ...
                     obj.GlobalParameters.EnableSpecialData*64 + ...
                     obj.GlobalParameters.ReduceDiscriminatorData*32 + ...
                     obj.GlobalParameters.EnablePhysicalData*16 + ...
                     obj.GlobalParameters.EnableDiscriminatorData*8 + ...
                     obj.GlobalParameters.DecodeGray*4 + ...
                     str2double(obj.configuration(1).EnableTacReadout)*2 + ...
                     emulate], ...
                        1, obj.nb_asics), ...
                 255])'; % end by 255


% * Save Header

if SaveDataFlag

    %% open file
    myrandom   = dec2hex(fix(mod(rand*now, 65530)), 4);
    mydir      = [getcurrentdir filesep sprintf('data_%s.bin', myrandom)];
    myfilename = sprintf('SaveData_%s.bin', myrandom);
    if ~ (exist(mydir, 'dir') == 7)
        mkdir(mydir)
    end
    fprintf('\n\tSaving data to %s\n', myfilename)
    fid = fopen( fullfile(mydir, myfilename), 'w', 'b');

    %% copy config files to data saving dir
    for i = 1:obj.nb_asics
        copyfile(fullfile('configuration', sprintf('catiroc_parameters_%i.param', i)), ...
                 fullfile(mydir))
    end
    copyfile(fullfile('configuration', 'global_parameters.param'), ...
             fullfile(mydir))

    %% read firmware ID and save to disk as for
    %% https://catiroc-test.gitlab.io/gui/#outline-container-orgd95b115
    AcqRawDataFormatID = obj.GetAcqRawDataFormatID;
    fwrite(fid, AcqRawDataFormatID ,'uint8');

    %% save acq start time
    c = clock;
    fwrite(fid, c(1) ,'uint16');
    fwrite(fid, c(2) ,'uint8');
    fwrite(fid, c(3) ,'uint8');
    fwrite(fid, c(4) ,'uint8');
    fwrite(fid, c(5) ,'uint8');
    fwrite(fid, c(6) ,'uint8');

    %% save nb. of asics
    fwrite(fid, obj.nb_asics ,'uint8');

    %% save command send to start acquisition
    fwrite(fid, Command ,'uint8');

    %% save binary configuration
    %% [6, nb_asics * 66, 255]
    fwrite(fid, obj.configuration_array ,'uint8');

end

% * Pointers to be used during acquisition

lpBufferPtr_big = libpointer('voidPtr', zeros(AmountInRxQueue, 1, 'uint8'));
el_read_ptr     = libpointer('ulongPtr',uint32(0));

% * Send command to trigger acquisition

obj.PurgeUSB;
obj.SendCommand(Command);

% * Start acquisition loop

LoopCounter = 1;

while get(gcbo,'Value')

% ** Ask for data

    [status, ~, el_read] = calllib('libftdi', 'ReadUSB', ...
                                   obj.DevNumber, ...
                                   lpBufferPtr_big, ...
                                   AmountInRxQueue, ...
                                   el_read_ptr);

% ** Check if received data is enough

    if AmountOfEmptyDataPacketsReceived == 3 && status==50  % No data at all: stop
        fprintf('\n\tError: No data (%2i bytes read) !!. \n\nStopped.\n', el_read),
        if ~SaveDataFlag || (SaveDataFlag && SaveAndDisplay)
            if get(handles.adc_time_tag, 'Value')
                set(HistogramADCTimeFigure, 'Visible', 'on')
            else
                set(HistogramADCTimeFigure, 'Visible', 'off')
            end
             if get(handles.adc_charge_tag, 'Value')
                set(HistogramADCChargeFigure, 'Visible', 'on')
            else
                set(HistogramADCChargeFigure, 'Visible', 'off')
            end
            set(HistogramADCTimeFigure, 'CloseRequestFcn', 'closereq')
            set(HistogramADCChargeFigure, 'CloseRequestFcn', 'closereq')
        end
        return
    elseif status==50  % No data at all
        fprintf('\n\tError: No data (%2i bytes read) !!\n', el_read),
        AmountOfEmptyDataPacketsReceived = AmountOfEmptyDataPacketsReceived + 1;
        drawnow
        continue
    elseif status ~= 0 % low data
        fprintf('\n\tWarning: %s, bytes read is %i; ignoring data\n', obj.status_chain(status), el_read),
        drawnow
        continue
    end
    AmountOfEmptyDataPacketsReceived = 0;

% ** Save data if necessary
    if SaveDataFlag
         fwrite(fid, lpBufferPtr_big.Value, 'uint8');
         %% In saving data mode, do nothing else when SaveAndDisplay flag is 0
         if ~SaveAndDisplay
             drawnow
             continue
        end
    end

% ** Decode data

    %% Reshape incoming data in 10 bytes events
    data = uint32(reshape(lpBufferPtr_big.Value, 10, [])');
 
    tmp_ = find(data(:,10) ~= obj.GlobalParameters.BoardId);
    if ~isempty(tmp_)
        data(tmp_, :) = [];
    end

    %% left most 32 bits in a single word
    Biggest32BitsWord = ...
        data(:,1)*2^24 + ...
        data(:,2)*2^16 + ...
        data(:,3)*2^8 + ...
        data(:,4);

    %% right most 32 bits in a single word
    Lowest32BitsWord = ...
        data(:,5)*2^24 + ...
        data(:,6)*2^16 + ...
        data(:,7)*2^8 + ...
        data(:,8);

    AsicNb = data(:,9);

    %% two leftmost bits
    EventKind = uint8(bitshift(Biggest32BitsWord, -30));

% *** Physical Events

    EventKindPhysicalIndex = (EventKind==0);

    if any(EventKindPhysicalIndex)

        tmp        = Biggest32BitsWord(EventKindPhysicalIndex);

        %% Remove fake events
        if isfield(obj.GlobalParameters, "IgnoreFakeEvents") && ...
                obj.GlobalParameters.IgnoreFakeEvents
            FakeIndicesToRemove = find(tmp==0, 16, 'first');
            tmp(FakeIndicesToRemove) = [];
            AsicNb_ = AsicNb(EventKindPhysicalIndex);
            AsicNb_(FakeIndicesToRemove) = [];
        else
            AsicNb_ = AsicNb(EventKindPhysicalIndex);
        end

        CoarseTime = bitand(tmp, uint32(2^26-1));

        Channel    = 16*AsicNb_ + bitand(bitshift(tmp, -26), uint32(15)) + 1;

        tmp        = Lowest32BitsWord(EventKindPhysicalIndex);

        %% Remove fake events
        if isfield(obj.GlobalParameters, "IgnoreFakeEvents") && ...
                obj.GlobalParameters.IgnoreFakeEvents
            tmp(FakeIndicesToRemove) = [];
        end

        FineTime     = bitand(tmp, uint32(2^10-1));

        Charge       = bitand(bitshift(tmp, -10), uint32(2^10-1));

        EventCounter = bitand(bitshift(tmp, -20), uint32(2^11-1));

        if todisplay

            Gain = bitshift(tmp, -31);

            [~,b] = sort(Channel(1:todisplay), 'ascend');

            PhysicalEvents = [Channel(b), ...
                              EventCounter(b), ...
                              CoarseTime(b), ...
                              Gain(b), ...
                              Charge(b), ...
                              FineTime(b)];

        end

    else

        CoarseTime   = -1;
        Channel      = -1;
        ADC_Charge   = -1;
        ADC_Time     = -1;
        EventCounter = -1;
        Gain         = -1;

    end

% *** Discriminator Events

    EventKindDiscriminatorIndex = (EventKind==2);

    if todisplay && any(EventKindDiscriminatorIndex)

        tmp                = Biggest32BitsWord(EventKindDiscriminatorIndex);

        DiscriEventCounter = bitand(tmp, uint32(2^25-1));

        DiscriChannel      = uint8(16*AsicNb(EventKindDiscriminatorIndex) + bitand(bitshift(tmp, -25), uint32(15)));

        DiscriEdge         = logical(bitand(bitshift(tmp, -29), uint32(1)));

        tmp                = Lowest32BitsWord(EventKindDiscriminatorIndex);

        DiscriFineTime     = uint8(bitand(tmp, uint32(2^6-1)));

        DiscriCoarseTime   = bitshift(tmp, -6);

        [~,b] = sort(DiscriChannel(1:todisplay), 'ascend');

        DiscriEvents = [Discri_Channel(b), ...
                        Discri_EventCounter(b), ...
                        TimeStamp(b), ...
                        Discri_Edge(b)];

    else

        DiscriEvents = 0;

    end

% *** Special Events

    EventKindSpecialIndex = (EventKind==1);

    if todisplay && any(EventKindSpecialIndex)

        TriggerRate = double(bitand(Lowest32BitsWord(EventKindSpecialIndex), uint32(2^16-1)))/10;
        TriggerRateChannel = double(bitand(bitshift(Lowest32BitsWord(EventKindSpecialIndex), -16), uint32(31)) + 1);

        if length(TriggerRate) < 32
            warning('\n\tNumber of Special events received is %i', length(TriggerRate))
            TriggerRate = 0;
        else
            ValidChannels = find(TriggerRate>0);
            TriggerRate = TriggerRate(ValidChannels);
            TriggerRateChannel = TriggerRateChannel(ValidChannels);
        end

    end

% ** Compute histograms

    all_channels = unique(Channel);

    for i = 1:length(all_channels)

        ch_nb = all_channels(i);
        index1 = (Channel == (ch_nb)) & (mod(EventCounter,2) == 1);
        index2 = (Channel == (ch_nb)) & (mod(EventCounter,2) == 0);
        if todisplay
            NbEventsByChannel(ch_nb) =  NbEventsByChannel(ch_nb) + sum(Channel == ch_nb);
        end

        HistogramADCTime(:, 2*ch_nb-1) = HistogramADCTime(:, 2*ch_nb-1) + ...
            uint64(hist(FineTime(index1)+1, 1:1024))';

        HistogramADCTime(:, 2*ch_nb) = HistogramADCTime(:, 2*ch_nb) + ...
            uint64(hist(FineTime(index2)+1, 1:1024))';

        HistogramADCCharge(:, 2*ch_nb-1) = HistogramADCCharge(:, 2*ch_nb-1) + ...
            uint64(hist(Charge(index1)+1, 1:1024))';

        HistogramADCCharge(:, 2*ch_nb) = HistogramADCCharge(:, 2*ch_nb) + ...
            uint64(hist(Charge(index2)+1, 1:1024))';

    end

% ** Update plots when necessary

    if get(handles.adc_time_tag, 'Value')
        %% Update plot contents
        for i = 1:length(all_channels)
            ch_nb = all_channels(i);
            set(PlotHandleHistogramADCTime(2*ch_nb-1), ...
                'YData', uint64(i*100) + HistogramADCTime(:, 2*ch_nb-1))
            set(PlotHandleHistogramADCTime(2*ch_nb), ...
                'YData', uint64(i*100) + HistogramADCTime(:, 2*ch_nb))
        end
        %% Display time figure
        if get(handles.acq_log_tag, 'Value')
            set(HistogramADCTimeFigureAxis, 'YScale', 'log')
        else
            set(HistogramADCTimeFigureAxis, 'YScale', 'linear')
        end
        set(HistogramADCTimeFigure, 'Visible', 'on')
    else
        set(HistogramADCTimeFigure, 'Visible', 'off')
    end

    if get(handles.adc_charge_tag, 'Value')
        %% Update plot contents
        for i = 1:length(all_channels)
            ch_nb = all_channels(i);
            set(PlotHandleHistogramADCCharge(2*ch_nb-1), ...
                'YData', HistogramADCCharge(:, 2*ch_nb-1))
            set(PlotHandleHistogramADCCharge(2*ch_nb), ...
                'YData', HistogramADCCharge(:, 2*ch_nb))
        end
        %% Display charge figure
        if get(handles.acq_log_tag, 'Value')
            set(HistogramADCChargeFigureAxis, 'YScale', 'log')
        else
            set(HistogramADCChargeFigureAxis, 'YScale', 'linear')
        end
        set(HistogramADCChargeFigure, 'Visible', 'on')
    else
        set(HistogramADCChargeFigure, 'Visible', 'off')
    end

% ** Put some information on screen if user requests it

    if todisplay

% 4. Display infos once by second
        tt = toc;
        if tt > 1

            fprintf(['\n\t\t____ First ToDisplay Physical / First ToDisplay ' ...
                     'DDS Events (time in us.) ______________________________\n\n'])

            if DiscriEvents == 0
                Datos = PhysicalEvents;
                fprintf(['\tChannel %2i \t  EventNb %4i \tTime %8i \tGain ' ...
                         '%1i\t  ADC_Charge %4i \tADC_Time %4i <-Phy \n'], Datos')
            else
                Datos = [PhysicalEvents, DiscriEvents];
                fprintf(['\tChannel %2i \t  EventNb %4i \tTime %8i \tGain ' ...
                         '%1i\t  ADC_Charge %4i \tADC_Time %4i <-Phy * DDS-> \tChannel %2i\tEventNb ' ...
                         '%4i\tTime %8i\tEdge %1i\n'], Datos')
            end

            if TriggerRate

                fprintf('\n\t\t**** Data Rate + Special Events ******************************\n\n')
                Datos = [double(all_channels), ...
                         NbEventsByChannel(all_channels)*8/(tt*1024*1024), ...
                         fix(NbEventsByChannel(all_channels)/tt), ...
                         TriggerRateChannel, ...
                         TriggerRate];
                fprintf('\t\t\tChannel %2i: [%2.3f MB/s => %5i Cts/s]\t Channel %2i: discri rate %8.2f (kHz.)   \n', Datos')

            else

                fprintf('\n\t\t**** Data Rate ******************************\n\n')
                Datos = [double(all_channels), ...
                         NbEventsByChannel(all_channels)*8/(tt*1024*1024), ...
                         fix(NbEventsByChannel(all_channels)/tt)];
                fprintf('\t\t\tChannel %2i: [%2.3f MB/s = %5i Cts/s]\n', Datos')

            end

            TotalRate = LoopCounter*AmountInRxQueue/tt;
            PhysicalRate = 8*sum(NbEventsByChannel(all_channels))/tt;
            set(MainGuiFigureHandle ,'Name', ...
                              sprintf('%2.2f/%2.2f MB/s -> %5i/%5i Cts/s', ...
                                      TotalRate/(1024*1024), ...
                                      PhysicalRate/(1024*1024),...
                                      fix(TotalRate/8),...
                                      fix(PhysicalRate/8)))

            NbEventsByChannel = zeros(16, 1);

            tic
            drawnow
            LoopCounter = 1;

        else % tt < 1

            LoopCounter = LoopCounter + 1;

        end% end tt > 1

    end % todisplay

    if ~todisplay
        tt = toc;
        if tt > 1
            tic
            drawnow
        end
    end

end % end while

% * End of acquisition

if ~SaveDataFlag || (SaveDataFlag && SaveAndDisplay)

    %% Make figures visible

    set(HistogramADCChargeFigure, 'Visible', 'on')
    set(HistogramADCTimeFigure, 'Visible', 'on')

    %% Enable figure closing by user

    set(HistogramADCChargeFigure, 'CloseRequestFcn', 'closereq')
    set(HistogramADCTimeFigure, 'CloseRequestFcn', 'closereq')

    %% Set a uicontext to save data and to run ipeak
    figure(HistogramADCChargeFigure), axis tight, zoom off
    set(gca, 'XLim', [max(find(sum(HistogramADCCharge,2)>1, 1, 'first')-10, 1) ...
        min(find(sum(HistogramADCCharge,2)>1, 1, 'last')+10, 1024)])
    for ii = 1:length(PlotHandleHistogramADCCharge)
        uic = uicontextmenu;
        uimenu ( uic, 'Label',sprintf('Save data ...'), 'Callback', 'save_data');
        set ( PlotHandleHistogramADCCharge(ii), 'uicontextmenu', uic);
        uic2 = uicontextmenu;
        uimenu ( uic2, 'Label',sprintf('IPeak_Channel_%i', ii), 'Callback', 'RunIpeak', 'Parent', uic);
    end

    %% Set a uicontext to save data and to run ipeak
    figure(HistogramADCTimeFigure), axis tight, zoom off
    for ii = 1:length(PlotHandleHistogramADCTime)
        uic = uicontextmenu;
        uimenu ( uic, 'Label',sprintf('Save data ...'), 'Callback', 'save_data');
        set ( PlotHandleHistogramADCTime(ii), 'uicontextmenu', uic);
        uic2 = uicontextmenu;
        uimenu ( uic2, 'Label',sprintf('IPeak_Channel_%i', ii), 'Callback', 'RunIpeak', 'Parent', uic);
    end

end

% * Close the file handle if necessary

if SaveDataFlag
    fclose(fid);
end


% * Display infos

if SaveDataFlag
    fprintf('\n\t\t***************************************\n')
    fprintf('\n\t\tDONT forget to fill-in the LogBook file\n')
    fprintf('\n\t\t***************************************\n')
end
