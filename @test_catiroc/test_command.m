function obj = test_command(obj, NbPackages, Order)
% TEST_COMMAND - Check data validity of command ORDER
%
% Request 'NbPackages' data packages of 65536 bytes each,
% containing a 8 bits up counter, and compare with reference
% to check data transfer

% Reference data package to compare incoming data with
Reference = 0:255;
Reference = repmat(Reference, 256, 1 )';

% Parse config file
% obj = obj.UpdateConfig;

% Purge USB buffer
obj.PurgeUSB

% Command, card will send NbPackages blocks of 65536 bytes
InitData = uint8([Order,0,NbPackages,0,0,255])';

% Send command to the card
obj.SendCommand(InitData);

% Check bytes available in USB fifo
AmountInRxQueue = obj.GetStatusUSB;
if AmountInRxQueue ~= 65536
    error('wrong number of bytes returned')
end

% Create pointer to read data
InitData_   = uint8([5; 250*zeros(AmountInRxQueue-2,1); 255]);
lpBufferPtr = libpointer('voidPtr', InitData_);

% Create pointer to store amount of data read
el_read_ptr = libpointer('ulongPtr',uint32(0));

% Lopp
for i = 1:NbPackages

    [~,~,~] = calllib('libftdi', 'ReadUSB', 0, ...
                      lpBufferPtr, length(lpBufferPtr.Value), el_read_ptr);

    DataRead = reshape(get(lpBufferPtr, 'Value')', 256, 256);

    if isequal(DataRead, Reference)
        fprintf('\n\tLoop %i, test OK.\n', i)
    else
        fprintf('\n\tLoop %i, test Failed.\n', i)
% TODO: Display statistics of errors
    end

end
