function el_written = SendCommand(obj, Command)
% SENDCOMMAND - Send a command, with parameters, to the board

%    SENDCOMMAND - Send a command to the board

    if GetIsOpen(obj)

        obj.PurgeUSB  % send null command and remove previous data in bus

        el_written_ptr = libpointer(obj.pointer_type,uint64(0));

        calllib('libftdi', 'WriteUSB', ...
                obj.DevNumber, ...
                libpointer('voidPtr', uint8(Command)), ...
                length(Command), ...
                el_written_ptr);

        el_written = el_written_ptr.value;

        if el_written ~= length(Command)
            error('SENDCOMMAND: wrong number of bytes sent to board')
        end

        pause(.1)

    else

        error('SENDCOMMAND: Device not open.')

    end