function obj = UpdateConfig(obj)
% UPDATECONFIG - Parse configuration file and update object properties

%    UPDATECONFIG - Reads default configuration file, copying its contents to the 'properties' structure
%    and updating 'configurationarray' binary array

    if obj.GetIsOpen

        for i = 1:obj.nb_asics

            %% i) Parameters are defined in a txt external file so that they may be changed by the user
            myfile = fullfile(getcurrentdir, 'configuration', sprintf('catiroc_parameters_%i.param', i));

            if ~(exist(myfile, 'file') == 2)
                fprintf('\n\tNo %s file found. Program Aborted.\n', myfile)
                return
            else
                fid = fopen(myfile, 'r');
                while ~feof(fid)
                    linea = fgetl(fid);
                    if isempty(linea),       continue,  end
                    if strcmp(linea(1),'#'), continue,  end
                    PosEqual      = strfind(linea,'=');
                    PosSemiColom  = strfind(linea,';');
                    parameter = linea(1:PosEqual-2);
                    value     = linea(PosEqual+2:PosSemiColom-1);
                    eval(sprintf('config(i).%s = ''%s'';', parameter, value))
                end
                fclose(fid);
            end

            %% Override config in parameters file with inputs in varargin array
%             if varargin{1}{1} >= 0
%                 for i = 1:length(varargin{1})
%                     config.(varargin{1}{i}) = varargin{2}{i};
%                 end
%             end

            %% ii) copy contents to object configuration structure
            obj.configuration = config;

        end % for i = 1 : nb_asics

    else

        error('UPDATECONFIG: Device not open.')

    end
