function obj = test_bandwidth(obj, Order, NbTimes)
% TEST_BANDWIDTH - Check data transfert rate

%% Update contents of global params config file
obj.GlobalParameters = GetGlobalParameters;

% loops over this number
L = 200;

% Parse config file
% obj = obj.UpdateConfig;

% Purge USB buffer
obj.PurgeUSB

% Command, card will send L blocks of 65536 bytes
InitData = uint8([Order,fix(L+100/256),rem(L+100,256),0,0,255])';

% Send command to the card
obj.SendCommand(InitData);

% Check bytes available in USB fifo
AmountInRxQueue_ = obj.GetStatusUSB;
if AmountInRxQueue_ ~= 65536
    error('wrong number of bytes returned')
end

AmountInRxQueue = obj.GlobalParameters.AmountInRxQueue;

% Create pointer to read data
InitData_   = uint8([5; 250*zeros(AmountInRxQueue-2,1); 255]);
lpBufferPtr = libpointer('voidPtr', InitData_);

% Create pointer to store amount of data read
el_read_ptr = libpointer('ulongPtr',uint32(0));

% Do test NbTimes
for j = 1:NbTimes

    tic % Start time measurement
    for i = 1:L

        [~,~,~] = calllib('libftdi', 'ReadUSB', 0, lpBufferPtr, length(lpBufferPtr.Value), el_read_ptr);

    end

    fprintf('\n\tData rate: %3.1f MB/s.\n', (L*AmountInRxQueue)/(toc*1024*1024))

% Check bytes available in USB fifo
% Should be 0 as we have read all requested packages
%     AmountInRxQueue = obj.GetStatusUSB(obj.configuration.verbose);
%     if AmountInRxQueue ~= 0
%         error('data left !')
%     end

% Send command to the card
    obj.SendCommand(InitData);

end