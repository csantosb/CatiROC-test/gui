
* Table of Contents                                 :TOC:
:PROPERTIES:
:VISIBILITY: all
:END:

  - [[#getasicdata][GetASICData]]
   - [[#method-prototype][Method prototype]]
   - [[#prepare][Prepare]]
     - [[#check-handle][Check handle]]
     - [[#tic][Tic]]
     - [[#gui-handle][Gui handle]]
     - [[#config][Config]]
     - [[#init-variables][Init variables]]
       - [[#nb-events-by-channel][Nb Events by channel]]
       - [[#trigger-rate][Trigger Rate]]
       - [[#data-saving][Data saving]]
       - [[#constants][Constants]]
     - [[#init-histograms][Init Histograms]]
       - [[#init-figures-and-plots][Init Figures and plots]]
       - [[#requested-timing][Requested timing]]
       - [[#buffers][Buffers]]
     - [[#send-command][Send command]]
   - [[#main-loop][Main loop]]
     - [[#take-data][Take data]]
     - [[#empty-packets][Empty packets]]
     - [[#decode][Decode]]
       - [[#32-leftmost-bits][32 leftmost bits]]
       - [[#32-rightmost-bits][32 rightmost bits]]
     - [[#save-data][Save Data]]
     - [[#check-data-validity][Check Data Validity]]
     - [[#channel-loop][Channel loop]]
       - [[#update-time-histograms][Update time histograms]]
       - [[#update-charge-histograms][Update charge histograms]]
       - [[#refresh-figures][Refresh figures]]
       - [[#update-switching-array][Update switching array]]
     - [[#end-channel-loop][End channel loop]]
     - [[#update-plot-windows][Update plot windows]]
     - [[#display][Display]]
       - [[#display-data-rate-by-channel][Display data rate by channel]]
        - [[#measure-time--to-compute-data-rate][Measure time :: to compute data rate.]]
     - [[#end-main-loop][End main loop]]
   - [[#end][End]]

*  GetASICData
:PROPERTIES:
:tangle:   GetASICData.m
:END:

** Method prototype

#+begin_src matlab
  function GetASICData(obj, emulate, handles)
  % GEASICTDATA - Read data from ASIC and display in terminal

  %   GETASICDATA - Obtains a packet of data from the ASIC and prints first TODISPLAY events
  %
  %   GEASICTDATA( EMULATE, FRAMEMODE, DECODE) obtains the data following
  %   next indications
  %   EMULATE - don't take real data, but emulated; useful for debugging
% TODISPLAY - print information regarding this number of events by received data packet
#+end_src

** Prepare

*** Check handle

#+begin_src matlab
  if not(GetIsOpen(obj))

      error('GEASICTDATA: Device not open.')

  end
#+end_src

*** Tic

#+begin_src matlab
  tic
#+end_src

*** Gui handle

Get handle to main gui to update its name

#+begin_src matlab
  MainGuiFigureHandle = get(get(gcbo, 'Parent'), 'Parent');
#+end_src

*** Config

Config ASIC with contents of configuration file: no override any of its
parameters.

#+begin_src matlab
  obj = obj.ConfigureASIC(handles, {-1}, {0});
#+end_src

*** Init variables

**** Nb Events by channel

TODO: update

Here I store “1” if first event, by channel, is to be considered as an even
(par) event, “0” otherwise (impar). Obviously, at first data package received,
first events by channel are all even. Upon reception of a new data package, I’ll
[[*Update switching array][update]] this array following the number of received events by channel.

This array is to be used afterwards when updating the histograms: I have to
consider odd and even data independently, so I need to know if first even in a
data package is even or odd. To this purpose, I need to keep track of number of
events in previous data package, checking if they are even or odd.

#+begin_src matlab
  NbEventsByChannel = zeros(1, 16);
#+end_src

**** Trigger Rate

TODO: update

Here I store “1” if first event, by channel, is to be considered as an even
(par) event, “0” otherwise (impar). Obviously, at first data package received,
first events by channel are all even. Upon reception of a new data package, I’ll
[[*Update switching array][update]] this array following the number of received events by channel.

This array is to be used afterwards when updating the histograms: I have to
consider odd and even data independently, so I need to know if first even in a
data package is even or odd. To this purpose, I need to keep track of number of
events in previous data package, checking if they are even or odd.

#+begin_src matlab
  TriggerRate = zeros(1, 16);
#+end_src

**** Data saving

#+begin_src matlab
  SaveDataFlag = get(handles.save_data_tag, 'Value');
  if SaveDataFlag
      myrandom   = dec2hex(fix(rand*6000), 4);
      mydir      = [getcurrentdir filesep sprintf('data_%s.bin', myrandom)];
      myfilename = sprintf('SaveData_%s.bin', myrandom);
      if ~ (exist(mydir, 'dir') == 7)
          mkdir(mydir)
      end
      fprintf('\n\tSaving data to %s\n', myfilename)
      fid = fopen( fullfile(mydir, myfilename), 'w');
      copyfile(fullfile('configuration', 'catiroc_parameters.param'), ...
               fullfile(mydir))
      copyfile(fullfile('configuration', 'global_parameters.param'), ...
               fullfile(mydir))
  end
#+end_src

**** Constants

#+begin_src matlab
  todisplay = handles.GlobalParameters.ToDisplay;
  % Randomize = handles.GlobalParameters.Randomize;
  % convert incoming data from gray to unsigned
  DecodeGray = handles.GlobalParameters.DecodeGray;
  AmountInRxQueue = handles.GlobalParameters.AmountInRxQueue;
  AmountOfEmptyDataPacketsReceived = 0;
#+end_src

*** Init Histograms

Two histograms by electronic channel, to account for odd (impar) and even (par)
events. Column 1 corresponds to odd events of channel 1; column 2 corresponds
to even events of channel 1, etc.

#+begin_src matlab
  HistogramADCCharge = zeros(2^10, 16*2, 'uint64');
  HistogramADCTime   = zeros(2^10, 16*2, 'uint64');
#+end_src

**** Init Figures and plots

Create figures to hold histogram data. Keep the handles to refresh contents at
run time.

#+begin_src matlab
  HistogramADCChargeFigure = figure('Visible', 'off', ...
                                    'CloseRequestFcn', ' ',  ...
                                    'Numbertitle', 'off', ...
                                    'Menubar', 'none', ...
                                    'Toolbar', 'figure', ...
                                    'Name', sprintf('%s', 'ADC Charge' ));

  hold on
  HistogramADCChargeFigureAxis = gca;
  PlotHandleHistogramADCCharge = plot(HistogramADCCharge);
  obj.SetColor(PlotHandleHistogramADCCharge)
  obj.SetFigure
#+end_src

**** Requested timing

Only in case the user asked to the ASIC to readout timing information, create an
additional figure. Keep the handles to refresh contents at run time.

#+begin_src matlab
  if str2double(obj.configuration.EnableTacReadout)

      HistogramADCTimeFigure = figure('Visible', 'off', ...
                                      'CloseRequestFcn', ' ',  ...
                                      'Numbertitle', 'off', ...
                                      'Menubar', 'none', ...
                                      'Toolbar', 'figure', ...
                                      'Name', sprintf('%s', 'ADC Fine Time' ));

      hold on
      HistogramADCTimeFigureAxis = gca;
      PlotHandleHistogramADCTime = plot(HistogramADCTime);
      obj.SetColor(PlotHandleHistogramADCTime)
      obj.SetFigure

  end
#+end_src

**** Buffers

Pre allocate memory for taking data, init buffers to allocate incoming data

#+begin_src matlab
  lpBufferPtr_big = libpointer('voidPtr', zeros(AmountInRxQueue, 1, 'uint8'));
  el_read_ptr     = libpointer('ulongPtr',uint32(0));
#+end_src

*** Send command

Send command "06" to request data with parameter “EnableTacReadout” from
configuration, to read data from ASIC considering second frame as 21 bits or 11
bits long. “Decode” forces performing decoding from gray to binary at runtime in
the fpga, and “emulate” ignores data from the ASIC, and generates data
instead. The latter is useful solely for debugging the data capture firmware.

First create the command

#+begin_src matlab
  ForceTrigger = get(handles.force_trigger_tag, 'Value');
  ForceTriggerValue = str2double(get(handles.force_trigger_value_tag, 'String'));

  Command = uint8([6, ...
                   ForceTriggerValue*16 + ...
                   ForceTrigger*8 + ...
                   DecodeGray*4 + ...
                   obj.configuration.EnableTacReadout*2 + ...
                   emulate, ...
                   255])';
#+end_src

then send

#+begin_src matlab
  obj.SendCommand(Command);
#+end_src

#+begin_src matlab
  LoopCounter = 1;
#+end_src

** Main loop

*** Take data

Start taking data while current base object “Value” property is set.

There exists a timeout to wait for data set during initialization of usb link.

#+begin_src matlab
  while get(gcbo,'Value')

      [status, ~, el_read] = calllib('libftdi', 'ReadUSB', ...
                                     obj.DevNumber, ...
                                     lpBufferPtr_big, ...
                                     AmountInRxQueue, ...
                                     el_read_ptr);
#+end_src

*** Empty packets

In case of no data, react accordingly.

“ReadUSB” returns ~= 0 if el_read ~= AmountInRxQueue

#+begin_src matlab
  if AmountOfEmptyDataPacketsReceived == 3 && status==50  % No data at all: stop
      fprintf('\n\tError: No data !!. \n\nStopped.\n'),
      if str2double(obj.configuration.EnableTacReadout)
          if get(handles.adc_time_tag, 'Value')
              set(HistogramADCTimeFigure, 'Visible', 'on')
          else
              set(HistogramADCTimeFigure, 'Visible', 'off')
          end
      end
      if get(handles.adc_charge_tag, 'Value'),
          set(HistogramADCChargeFigure, 'Visible', 'on')
      else
          set(HistogramADCChargeFigure, 'Visible', 'off')
      end
      set(HistogramADCTimeFigure, 'CloseRequestFcn', 'closereq')
      set(HistogramADCChargeFigure, 'CloseRequestFcn', 'closereq')
      return
  elseif status==50  % No data at all
      fprintf('\n\tError: No data !!\n'),
      AmountOfEmptyDataPacketsReceived = AmountOfEmptyDataPacketsReceived + 1;
      drawnow
      continue
  elseif status ~= 0 % low data
      fprintf('\n\tWarning: %s, bytes read is %i; ignoring data\n', obj.status_chain(status), el_read),
      drawnow
      continue
  end
  AmountOfEmptyDataPacketsReceived = 0;
#+end_src

*** Decode

Decode incoming binary data to something meaningfull.

#+begin_src matlab
  data_ = reshape(lpBufferPtr_big.Value, 8, [])';
  data = uint64(data_);
#+end_src

**** 32 leftmost bits

The 32 bits word

#+begin_src matlab
  tmp = uint32(data(:,1)*2^24 + data(:,2)*2^16 + data(:,3)*2^8 + data(:,4));
#+end_src

Coarse time, 26 bits

#+begin_src matlab
  CoarseTime = double(bitand(tmp, uint32(2^26-1)));
  CoarseTime = CoarseTime*25e-9/1e-6';
#+end_src

Channel number, 4 bits

#+begin_src matlab
  tmp = bitshift(tmp, -26);
  Channel = bitand(tmp, uint32(15)) + 1;
#+end_src

Gain, 1 bit

#+begin_src matlab
  tmp = bitshift(tmp, -4);
  Gain = bitand(tmp, uint32(1));
#+end_src

Event kind, 1 bit

#+begin_src matlab
  EventKind = bitshift(tmp, -1);
  EventKindIndex = find(EventKind);
  if not(isempty(EventKindIndex))
      tmp = uint32(data(EventKindIndex,5)*2^24 + ...
                   data(EventKindIndex,6)*2^16 + ...
                   data(EventKindIndex,7)*2^8 + ...
                   data(EventKindIndex,8));
      TriggerRate_ = double(bitand(tmp, uint32(2^11-1)));
      tmp = uint32(data(EventKindIndex,1)*2^24 + ...
                   data(EventKindIndex,2)*2^16 + ...
                   data(EventKindIndex,3)*2^8 + ...
                   data(EventKindIndex,4));
      tmp = bitshift(tmp, -26);
      TriggerRateChannel = bitand(tmp, uint32(15)) + 1;
      if isequal(TriggerRateChannel, (1:16)')
          TriggerRate = TriggerRate_;
      else
          TriggerRate = -1*ones(1,16);
      end
  end
#+end_src

**** 32 rightmost bits

The 32 bits word

#+begin_src matlab
  tmp = uint32(data(:,5)*2^24 + data(:,6)*2^16 + data(:,7)*2^8 + data(:,8));
#+end_src

ADC Time, 10 bits

#+begin_src matlab
  ADC_Time  = bitand(tmp, uint32(2^10-1));
#+end_src

ADC Charge, 10 bits
Decode timing if requested.

#+begin_src matlab
  %if str2double(obj.configuration.EnableTacReadout)
  tmp = bitshift(tmp, -10);
  ADC_Charge  = bitand(tmp, uint32(2^10-1));
  %end
#+end_src

Event Counter

#+begin_src matlab
  EventCounter = bitshift(tmp, -10);
#+end_src

#+begin_src matlab :tangle no
  % DataRate = uint16(data(:,5)*2^8 + data(:,6));
  % DataRate = bitshift(DataRate, -5);
#+end_src

*** Save Data

#+begin_src matlab
  if SaveDataFlag
      fwrite(fid, data_', 'uint8');
  end
#+end_src

*** Check Data Validity
 :tangle no
#+begin_src matlab
%             if find(abs(diff(CoarseTime)) > 100)
%                 figure,plot(CoarseTime)
%                 fprintf('aqui')
%            end

%             if length(find(Channel~=1 & Channel~=7 & Channel~=0 & Channel~=4))>4
%                 unique(Channel(Channel~=1 & Channel~=7 & Channel~=0 & Channel~=4))'
%                 fprintf('\n\tWarning\n')
%                 pause(2)
%             end
#+end_src

*** Channel loop

Proceed to process data by channel.

Here, “index” includes the positions of events in the array corresponding to
this channel.

#+begin_src matlab
  all_channels = unique(Channel);
  for i = 1:length(all_channels)

      ch_nb = all_channels(i);
      index1 = (Channel == (ch_nb)) & (EventKind == 0) & (mod(EventCounter,2) == 1);
      index2 = (Channel == (ch_nb)) & (EventKind == 0) & (mod(EventCounter,2) == 0);
      NbEventsByChannel(ch_nb) =  NbEventsByChannel(ch_nb) + sum((Channel == (ch_nb))  & (EventKind == 0));
#+end_src

**** Update time histograms

Time: even events

When “SwitchingArray” takes 0, means in previous data packet the number of
events corresponding to this channel where odd (impar): I take events as
“1:2:end”. Otherwise, I’ll take events as “2:2:end”

#+begin_src matlab
  if str2double(obj.configuration.EnableTacReadout)
      HistogramADCTime(:, 2*ch_nb-1) = HistogramADCTime(:, 2*ch_nb-1) + ...
          uint64(hist(ADC_Time(index1)+1, 1:1024))';
  end
#+end_src

Time: odd events

Similar to even events, except I negate the value of “SwitchingArray”. I take events as
“2:2:end”. Otherwise, I’ll take events as “1:2:end”

#+begin_src matlab
  if str2double(obj.configuration.EnableTacReadout)
      HistogramADCTime(:, 2*ch_nb) = HistogramADCTime(:, 2*ch_nb) + ...
          uint64(hist(ADC_Time(index2)+1, 1:1024))';
  end
#+end_src

**** Update charge histograms

TODO: implement as time histos

#+begin_src matlab
  if str2double(obj.configuration.EnableTacReadout)
      HistogramADCCharge(:, 2*ch_nb-1) = HistogramADCCharge(:, 2*ch_nb-1) + ...
          uint64(hist(ADC_Charge(index1)+1, 1:1024))';
  %         else
  %             HistogramADCCharge(:, 2*ch_nb-1) = HistogramADCCharge(:, 2*ch_nb-1) + ...
  %                 uint64(hist(ADC_Time(index2)+1, 1:1024))';
  end
#+end_src

#+begin_src matlab
  if str2double(obj.configuration.EnableTacReadout)
      HistogramADCCharge(:, 2*ch_nb) = HistogramADCCharge(:, 2*ch_nb) + ...
          uint64(hist(ADC_Charge(index2)+1, 1:1024))';
  %         else
  %             HistogramADCCharge(:, 2*ch_nb) = HistogramADCCharge(:, 2*ch_nb) + ...
  %                 uint64(hist(ADC_Time(index((not(SwitchingArray(ch_nb))+1):2:end))+1, 1:1024))';
  end
#+end_src

**** Refresh figures

Only refresh if time increased beyond some value

#+begin_src matlab
  if 1 %DisplayIt

      if str2double(obj.configuration.EnableTacReadout)
          set(PlotHandleHistogramADCTime(2*ch_nb-1), 'YData', HistogramADCTime(:, 2*ch_nb-1))
          set(PlotHandleHistogramADCTime(2*ch_nb), 'YData', HistogramADCTime(:, 2*ch_nb))
          if get(handles.acq_log_tag, 'Value')
              set(HistogramADCTimeFigureAxis, 'YScale', 'log')
          else
              set(HistogramADCTimeFigureAxis, 'YScale', 'linear')
          end
      end

      set(PlotHandleHistogramADCCharge(2*ch_nb-1), 'YData', HistogramADCCharge(:, 2*ch_nb-1))
      set(PlotHandleHistogramADCCharge(2*ch_nb), 'YData', HistogramADCCharge(:, 2*ch_nb))

      if get(handles.acq_log_tag, 'Value')
          set(HistogramADCChargeFigureAxis, 'YScale', 'log')
      else
          set(HistogramADCChargeFigureAxis, 'YScale', 'linear')
      end

  end
#+end_src

**** Update switching array

Check if amount of events in this data package was even of odd, and update
“SwitchingArray” accordingly.

#+begin_src matlab
  % if ~Randomize
  %     if ~ (ceil(length(index)/2) == length(index)/2)
  %         %% odd (impar)
  %         SwitchingArray(ch_nb) = 1;
  %     else
  %         %% even (par)
  %         SwitchingArray(ch_nb) = 0;
  %     end
  % end
  % if Randomize
  %     SwitchingArray(ch_nb) = not(SwitchingArray(ch_nb));
  % end
#+end_src

*** End channel loop

End of this loop, proceed to next channel.

#+begin_src matlab
  end % for channels
#+end_src

*** Update plot windows

Make the visible or not.

Time figure ...

#+begin_src matlab
  %if str2double(obj.configuration.EnableTacReadout)
      if get(handles.adc_time_tag, 'Value')
          set(HistogramADCTimeFigure, 'Visible', 'on')
      else
          set(HistogramADCTimeFigure, 'Visible', 'off')
      end
  %end
#+end_src

and charge figure ...

#+begin_src matlab
  if get(handles.adc_charge_tag, 'Value'),
      set(HistogramADCChargeFigure, 'Visible', 'on')
  else
      set(HistogramADCChargeFigure, 'Visible', 'off')
  end
#+end_src

*** Display

Print some information in terminal, but only if time increased beyond
some value

****

#+begin_src matlab
  tt = toc;
  if tt > 1

      DisplayIt = 1; % toc > .1;
      if todisplay && DisplayIt

          %% if decode timing if requested
          if str2double(obj.configuration.EnableTacReadout)

              Datos = [Channel(1:todisplay), ...
                       CoarseTime(1:todisplay), ...
                       Gain(1:todisplay), ...
                       ADC_Charge(1:todisplay), ...
                       ADC_Time(1:todisplay)];
              fprintf('\n\n')
              fprintf('\tChannel %2i \tCoarseTime (us) %8i \tGain %1i\tADC_Charge %4i \tADC_Time %4i\n', Datos')

          else

              Datos = [Channel(1:todisplay), ...
                       CoarseTime(1:todisplay), ...
                       Gain(1:todisplay), ...
                       ADC_Time(1:todisplay)];
              fprintf('\n\n')
              fprintf('\tChannel %2i \tCoarseTime (us) %8i \tGain %1i\tADC_Charge %4i\n', Datos')

          end

      end
#+end_src

**** Display data rate by channel

#+begin_src matlab
    if todisplay
        fprintf('\n\n')
        for i = 1:length(all_channels)
            ch_nb = all_channels(i);
            fprintf('\tChannel %2i    [%2.2f MB/s = %5i Cts/s] / %5.1f Trigger/s\n', ...
                    ch_nb, ...
                    NbEventsByChannel(ch_nb)*8/(tt*1024*1024), ...
                    fix(NbEventsByChannel(ch_nb)/tt), ...
                    TriggerRate(ch_nb)/10e-3)
        end
        NbEventsByChannel = zeros(1, 16);
    end
#+end_src

****  Measure time :: to compute data rate.

#+begin_src matlab
  TotalRate = LoopCounter*AmountInRxQueue/tt;
  set(MainGuiFigureHandle ,'Name', ...
                    sprintf('%2.2f MB/s - %5i Cts/s Total', ...
                            TotalRate/(1024*1024), ...
                            fix(TotalRate/8)))
#+end_src

Force refresh display

#+begin_src matlab
  tic
  drawnow
  LoopCounter = 1;
#+end_src

#+begin_src matlab
    else

      LoopCounter = LoopCounter + 1;

  end
#+end_src

*** End main loop

[[*Main loop][While]] loop

#+begin_src matlab
  end % end while
#+end_src

** End

#+begin_src matlab
  set(HistogramADCChargeFigure, 'CloseRequestFcn', 'closereq')
  if strcmp(get(HistogramADCChargeFigure, 'Visible'), 'on')
      figure(HistogramADCChargeFigure), axis tight, zoom off
      for ii = 1:length(PlotHandleHistogramADCCharge)
          uic = uicontextmenu;
          uimenu ( uic, 'Label',sprintf('Save data ...'), 'Callback', 'save_data');
          set ( PlotHandleHistogramADCCharge(ii), 'uicontextmenu', uic);
          uic2 = uicontextmenu;
          uimenu ( uic2, 'Label',sprintf('IPeak_Channel_%i', ii), 'Callback', 'RunIpeak', 'Parent', uic);
      end
  end

  set(HistogramADCTimeFigure, 'CloseRequestFcn', 'closereq')
  if strcmp(get(HistogramADCTimeFigure, 'Visible'), 'on')
      figure(HistogramADCTimeFigure), axis tight, zoom off
      for ii = 1:length(PlotHandleHistogramADCTime)
          uic = uicontextmenu;
          uimenu ( uic, 'Label',sprintf('Save data ...'), 'Callback', 'save_data');
          set ( PlotHandleHistogramADCTime(ii), 'uicontextmenu', uic);
          uic2 = uicontextmenu;
          uimenu ( uic2, 'Label',sprintf('IPeak_Channel_%i', ii), 'Callback', 'RunIpeak', 'Parent', uic);
      end
  end

  if SaveDataFlag
      fclose(fid);
      fprintf('\n\t\t***************************************\n')
      fprintf('\n\t\tDONT forget to fill-in the LogBook file\n')
      fprintf('\n\t\t***************************************\n')
  end
#+end_src
