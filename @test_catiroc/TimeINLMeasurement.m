function obj = TimeINLMeasurement(obj, handles)
% TIMEINLMEASUREMENT - Updates configuration of ADC through SPI

%   TIMEINLMEASUREMENT -
%   sends
%

    if not(GetIsOpen(obj))

        error('GEASICTDATA: Device not open.')

    end

    %% Config ASIC
    obj = obj.ConfigureASIC(handles, {-1}, {0});

    %% Params
    DecodeGray = obj.GlobalParameters.DecodeGray;
    ForceTrigger = get(handles.force_trigger_tag, 'Value');
    ForceTriggerValue = str2double(get(handles.force_trigger_value_tag, ...
                                       'String'));

    %% Pointers for data arrays

    AmountInRxQueue = obj.GlobalParameters.AmountInRxQueue;
    lpBufferPtr_big = libpointer('voidPtr', zeros(AmountInRxQueue, ...
                                                  1, 'uint8'));
    el_read_ptr     = libpointer('ulongPtr',uint32(0));

    %% Create directory to store data

    myrandom   = dec2hex(fix(mod(rand*now, 65530)), 4);
    mydir      = [getcurrentdir filesep sprintf('timingINL_%s', myrandom)];
    if ~ (exist(mydir, 'dir') == 7)
        mkdir(mydir)
    end
    fprintf('\n\tSaving timing to %s\n', sprintf('timingINL_%s', myrandom))
    copyfile(fullfile('configuration', 'catiroc_parameters.param'), fullfile(mydir))
    copyfile(fullfile('configuration', 'global_parameters.param'), fullfile(mydir))

    %% Loop on steps

    h = waitbar(0,'Please wait...');

    for i = 1:obj.GlobalParameters.TimingINL.steps

        fprintf('\n\tStep %i ... \n', i)
        waitbar(i/obj.GlobalParameters.TimingINL.steps, h)

        %% New file
        myfilename = sprintf('SaveData_delay_%i.bin', i);
        fid = fopen( fullfile(mydir, myfilename), 'w');

        %% Command to take data
        Command = uint8([6, ...
                         bitshift(i, -8), ...
                         bitand(i, 63), ....
                         ForceTriggerValue*16 + ...
                         ForceTrigger*8 + ...
                         DecodeGray*4 + ...
                         obj.configuration.EnableTacReadout*2 + ...
                         0, ... % emulate
                         255])';
        obj.SendCommand(Command);

        %% Take data during some time

        tic
        TimeElapsed = 0;

        while TimeElapsed < obj.GlobalParameters.TimingINL.time

            [status, ~, el_read] = calllib('libftdi', 'ReadUSB', ...
                                           obj.DevNumber, ...
                                           lpBufferPtr_big, ...
                                           AmountInRxQueue, ...
                                           el_read_ptr);

            if status ~= 0 % low data
                error('\n\tWarning: %s, bytes read is %i; ignoring data\n', obj.status_chain(status), el_read),
            end

            %% Data on disk
            fwrite(fid, reshape(lpBufferPtr_big.Value, 8, []), 'uint8');

            TimeElapsed = toc;
            drawnow
        end

        fclose(fid);

    end

    fprintf('\n\tDone !! Happy Analysis !!!! \n')
    close(h)